public class WordCountsStudTest {

    public static void printWca(WordCountsArray wca){
        int i = 0;
        for(i = 0; i < wca.size()-1; i++)
            System.out.print(wca.getWordCountArray()[i].getWord() + " " + wca.getWordCountArray()[i].getFrequency() + ", ");
        System.out.println(wca.getWordCountArray()[i].getWord() + " " + wca.getWordCountArray()[i].getFrequency());
        System.out.println();
    }


    public static void testSort(WordCountsArray wca){
        System.out.println("Before sorting:");
        printWca(wca);
        wca.sort();
        System.out.println("After sorting:");
        printWca(wca);
    }

    public static void testSimilarity(WordCountsArray wca1, WordCountsArray wca2){
        System.out.println("WordCountsArray 1:");
        printWca(wca1);
        System.out.println("WordCountsArray 2:");
        printWca(wca2);
        System.out.println("Similarity = " + wca1.computeSimilarity(wca2));
        System.out.println();
    }

    public static void testGetIndexOfWord(WordCountsArray wca, String word){
        System.out.println("Index of word " + "\"" + word + "\"" + " is " + wca.getIndexOfWord(word));
        System.out.println();
    }

    public static void testGetWord(WordCountsArray wca, int index){
        String w = wca.getWord(index);
        if(w!=null)
            System.out.println("At index " + index + " is the word " + "\"" + w + "\"");
        else
            System.out.println("Index " + index + " is invalid.");
        System.out.println();
    }

    public static void testAdd(WordCountsArray wca, String word, int count){
        System.out.println("WordCountsArray before adding: ");
        printWca(wca);
        System.out.println("Adding word " + "\"" + word + "\"" + " with count " + count);
        System.out.println("WordCountsArray after calling add(word, index): ");
        wca.add(word,count);
        printWca(wca);
    }
    public static void main(String[] args) {
        WordCountsArray a = new WordCountsArray(5);
        WordCountsArray b = new WordCountsArray(4);
        //WordCountsArray c = new WordCountsArray(0);

        a.add("Oy", 1);
        a.add("with", 1);
        a.add("the", 1);
        a.add("poodles", 1);
        a.add("already", 1);
        a.add("Oy", 1);
        a.add("is", 1);
        a.add("the", 1);
        a.add("funniest", 1);
        a.add("word", 1);
        a.add("except", 1);
        a.add("Poodle", 1);

        testAdd(a, "Poodles", 8);
        testAdd(a, "hello", -6);
        testAdd(a, "Lorelai", 0);

        for(int i = 0; i < a.size(); i++)
            b.add(a.getWordCountArray()[i].getWord(), a.getWordCountArray()[i].getFrequency());

        testGetIndexOfWord(a,"poodles");
        testGetIndexOfWord(a, "Rory");

        testGetWord(a, 3);
        testGetWord(a, 25);
        testGetWord(a, -6);

        testSimilarity(a,b);
        testAdd(b,"poodles", 10);
        testSimilarity(a,b);

        testSort(a);
    }
}
