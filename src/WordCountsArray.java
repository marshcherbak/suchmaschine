public class WordCountsArray {


    private WordCount [] wordCountArray;
    public WordCount[] getWordCountArray() {
        return wordCountArray;
    }
    public WordCountsArray(int initSize){
        if(initSize>0)
            this.wordCountArray = new WordCount[initSize];
        else
            this.wordCountArray = new WordCount[0];
    }

    /* adds word with count
    * if word is already in the array, ups count
    * if not, adds new words with frequency count
    * if word is null, == "" or count < 0, does nothing */
    public void add(String word, int count){
        if (word!=null && !word.equals("") && count>=0){
            word = word.toLowerCase();
            int check = getIndexOfWord(word);
            if(check!=-1){
                wordCountArray[check].incrementCount(count);
            }
            else {
                WordCount w = new WordCount(word, count);
                if (this.size() < this.wordCountArray.length) {
                    for (int i = 0; i < wordCountArray.length; i++) {
                        if (wordCountArray[i] == null) {
                            wordCountArray[i] = w;
                            break;
                        }
                    }
                } else {
                    WordCount[] temp = new WordCount[wordCountArray.length + 1];
                    for (int i = 0; i < wordCountArray.length; i++)
                        temp[i] = wordCountArray[i];
                    temp[temp.length - 1] = w;
                    wordCountArray = temp;
                }
            }
        }
    }

    // returns the amount of words in the wordCountArray (can be less than length)
    public int size(){
        int size = 0;
        for(int i = 0; i < wordCountArray.length; i++){
            if(wordCountArray[i]!=null)
                size++;
        }
        return size;
    }

    // returns word at index
    // if index illegal, returns null
    public String getWord(int index){
        if(index < this.size() && index >= 0)
            return wordCountArray[index].getWord();
        else
            return null;
    }

    //returns frequency of the word at index
    public int getCount(int index){
        if(index < this.size() && index >= 0)
            return wordCountArray[index].getFrequency();
        else
            return -1;
    }

    public void setCount(int index, int count){ //when count < 0, doesn't do anything
        if(index < this.size() && count >=0 && index >=0)
            wordCountArray[index].setFrequency(count);
    }

    /* compares this to wordcountsarray wca
    * if lengths are the same, all elements are equal and in the same order, true
    * else false*/
    public boolean equals(WordCountsArray wca){
        if(wca != null){
            if(wca.size() == this.size()){
                int i = 0;
                boolean flag = true;
                while (i < this.size()&& flag){
                    if (this.wordCountArray[i].equals(wca.getWordCountArray()[i]))
                        i++;
                    else
                        flag = false;
                }
                return i == this.size();
            }
        }
        return false;
    }

    /* returns index of word in the array
    * if the words isn't in the array, returns -1 */
    public int getIndexOfWord(String word){
        for(int i = 0; i < this.size(); i++){
            if(word.equals(wordCountArray[i].getWord()))
                return i;
        }
        return -1;
    }

    /*returns true of all words are equal and in the same order (frequency can be different)
    * else, false */
    private boolean wordsEqual(WordCountsArray wca){
        if(this.size() == wca.size()){
            boolean flag = true;
            int i = 0;
            while(i < this.size() && flag){
                if(!wordCountArray[i].getWord().equals(wca.wordCountArray[i].getWord()))
                    flag = false;
                else
                    i++;
            }
            return flag;
        }
        return false;
    }

    // scalar product of frequencies
    private double scalarProduct(WordCountsArray wca){
        if(!this.wordsEqual(wca) || wca == null)
            return 0;
        int result = 0;
        for(int i = 0; i < this.size(); i++){
            result+=this.wordCountArray[i].getFrequency()*wca.wordCountArray[i].getFrequency();
        }
        return result;
    }

    /* selection sort - the result is wordcountsarray sorted ascending */
    public void sort(){
       doSelectionSort();
        //doBucketSort();
    }

    /* computes similarity of vectors of frequencies by the given formula*/
    public double computeSimilarity(WordCountsArray wca){
        if(this.size()==wca.size() && wca!=null) {
            double a = this.scalarProduct(wca);
            double b = this.scalarProduct(this) * wca.scalarProduct(wca);
            double c = Math.sqrt(b);
            return a / c;
        }
        else
            return 0;
    }

    /* selection sort */
    private void doSelectionSort(){
        int counter = 0;

        while(counter < this.size()){ //until all sorted
            WordCount min = wordCountArray[counter]; // first - minimal
            int forSwap = counter;
            for(int i = counter; i < this.size(); i++){ //find new minimal
                if(wordCountArray[i].getWord().compareTo(min.getWord())<0){
                    min = wordCountArray[i];
                    forSwap = i;
                }
            }
            //move new minimal to the sorted part
            WordCount temp = wordCountArray[forSwap];
            wordCountArray[forSwap] = wordCountArray[counter];
            wordCountArray[counter]=temp;
            counter++; // increase counter
        }
    }

    /* bucket sort */
    private void doBucketSort(){
        WordCountsArray [] letters = new WordCountsArray[26]; // 26 letter in the english alphabet
        for(int ind = 0; ind < 26; ind++)
            letters[ind] = new WordCountsArray(0);
        // letters[x-97]

        int counter = 0;
        int index = 1;
        while(counter!=this.size()){
            counter = 0;
            for(int ind = 0; ind < this.size(); ind++){
                int at = this.getWord(ind).length()-index;
                if(at <= 0) { // smaller words stay in their "final" bucket (according to the first letter)
                    int here = this.getWord(ind).charAt(0) - 97;
                    letters[here].add(this.getWord(ind), this.getCount(ind));
                    counter++;
                }
                else if(at > 0) { // bucket number = asccii code for the letter - 97
                    int here = this.getWord(ind).charAt(at) - 97;
                    letters[here].add(this.getWord(ind), this.getCount(ind));
                }
            }
            if(counter==this.size()) { //when everything is in final buckets, sort each so that smaller words are in the right position
                for (int ind = 0; ind < 26; ind++)
                    letters[ind].doSelectionSort();
            }
            WordCountsArray wca = new WordCountsArray(0);
            int adding = 0;
            int inside = 0;
            while(adding < this.size()){ //get ordered words from buckets into a new WordCountsArray so that it can get sorted into buckets based on next symbol
                WordCountsArray wca2 = letters[inside];
                for(int ind = 0; ind < letters[inside].size(); ind++){
                    wca.add(letters[inside].getWord(ind), letters[inside].getCount(ind));
                    adding++;
                }
                inside++;
            }
            this.wordCountArray = wca.wordCountArray;
            for(int ind = 0; ind < 26; ind++) //empty letters
                letters[ind] = new WordCountsArray(0);
            index++;
        }
    }

    /* calculates weights of all the words in this according to the formula
    *  if dc is null or empty does nothing
    *  else assigns all calculated weight to the according attribute of WordCount*/
    private void calculateWeights(DocumentCollection dc){
        if(dc==null || dc.isEmpty())
            return;
        for(int i = 0; i < this.size(); i++){
            String w = this.getWord(i);
            double invF = this.invertedFrequency(w,dc);
            double c = this.getCount(i);
            double weight = invF*c;
            this.wordCountArray[i].setWeight(weight);
        }
    }

    /* calculates inverted frequency according to the formula
    * if word or dc are null or dc has no documents with the word returns 0 */
    private double invertedFrequency(String word, DocumentCollection dc){
        double res = 0;
        if(word!=null && dc != null) {
            double a = (double) dc.numDocuments() + 1;
            double b = (double) dc.noOfDocumentsContainingWord(word);

            if (b != 0)
                res = Math.log(a / b);
        }
        return res;
    }

    /* calculates normalized weights and assigns them to the according attribute
    * if dc is null or empty does nothing
    * nWeight can be NaN*/
    private void calculateNormalizedWeights(DocumentCollection dc){
        if(dc==null || dc.isEmpty())
            return;
        calculateWeights(dc);

        double sum = 0;
        for(int i = 0; i < this.size(); i++){
            sum+=Math.pow(wordCountArray[i].getWeight(),2);
        }
        sum = Math.sqrt(sum);

        for(int i = 0; i < this.size(); i++){
            double a = wordCountArray[i].getWeight();
            double nWeight = 0;
            if(sum!=0)
                nWeight = a/sum;
            wordCountArray[i].setNormalizedWeight(nWeight);
        }
    }

    /* assume arrays are sorted and have the same words (since the method is for document collection)
    * calculates the scalar product of normalized weights
    * if dc or wca are null returns 0
    * can return NaN (some weights can be NaN) */
    private double scalarProduct(WordCountsArray wca, DocumentCollection dc){
        if(dc==null || wca == null)
            return 0;
        this.calculateNormalizedWeights(dc);
        wca.calculateNormalizedWeights(dc);
        double res = 0;
        if(this.size()==wca.size()){
            for(int i = 0; i < this.size(); i++){
                res+=this.wordCountArray[i].getNormalizedWeight()*wca.wordCountArray[i].getNormalizedWeight();
            }
        }
        return res;
    }

    /* assume arrays have the same words
    * if dc or wca are null returns 0
    * if arrays hae different sizes returns 0
    * else returns scalar product of normalized weights */
    public double computeSimilarity(WordCountsArray wca, DocumentCollection dc){
        if(wca==null || dc == null )
            return 0;
        if(this.size() == wca.size())
            return scalarProduct(wca,dc);
        else
            return 0;
    }
}
