import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TestItServer {

    private static void addTest(LinkedDocumentCollection dc, String command){
        int j = 0;
        String title = "";
        while(command.charAt(j)!=':'){
            title += command.charAt(j);
            j++;
        }
        String content = command.substring(j+1);
        LinkedDocument d = new LinkedDocument(title, "en", "sum", new Date(),
                new Author("FN", "LN", new Date(), "res", "@"),content, title);
        dc.appendDocument(d);
    }

    private static String listTest(LinkedDocumentCollection dc){
        DocumentCollectionCell temp = dc.getDocuments();
        String res = "";
        while(temp!=null){
            res += temp.getInfo().getTitle()+Terminal.NEWLINE;
            temp = temp.getNext();
        }
        return res;
    }

    private static String countTest(LinkedDocumentCollection dc, String command){
        int k = 0;
        String word = "";
        while(k < command.length()){
            word += command.charAt(k);
            k++;
        }
        DocumentCollectionCell temp2 = dc.getDocuments();
        String res = "";
        while(temp2!=null){
            WordCountsArray wc = temp2.getInfo().getWordCounts();
            int check = wc.getIndexOfWord(word);
            if(check==-1)
                res+="\"" + word + "\"" + " in " + "\"" + temp2.getInfo().getTitle() + "\" : " + "not at all." + Terminal.NEWLINE;
            else {
                int count = wc.getCount(check);
                res+="\"" + word + "\"" + " in " + "\"" + temp2.getInfo().getTitle() + "\" : x" + count + "."+Terminal.NEWLINE;
            }
            temp2 = temp2.getNext();
        }
        return res;
    }

    private static String queryTest(LinkedDocumentCollection dc, String search){
        dc.match(search);
        DocumentCollectionCell temp3 = dc.getDocuments();
        int l = 1;
        String res = "";
        while(temp3!=null){
            res += l + ". " + temp3.getInfo().getTitle() + ";    Relevance = " + dc.getRelevance(l-1)+Terminal.NEWLINE;
            temp3 = temp3.getNext();
            l++;
        }
        return res;
    }



    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8000);

        try {
            while (true) {
                Socket client = serverSocket.accept();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                PrintWriter socketWriter = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));

                String command = "";
                LinkedDocumentCollection dc = new LinkedDocumentCollection();

                while (!command.equals("exit")) {
                    socketWriter.print('>');
                    socketWriter.flush();
                    command = bufferedReader.readLine();
                    if (command == null) // Verbindung geschlossen
                        break;

                    String result = "";
                    String s = "";
                    int i = 0;
                    while(i < command.length() && command.charAt(i)!=' '){
                        s+=command.charAt(i);
                        i++;
                    }
                    if(i<command.length()-1)
                        command = command.substring(i+1);
                    switch(s){
                        case "exit":
                            return;
                        case "add":
                            addTest(dc, command);
                            break;
                        case "list":
                            result = listTest(dc);
                            break;
                        case "count":
                            result = countTest(dc, command);
                            break;
                        case "query":
                            result = queryTest(dc, command);
                            break;
                        case "crawl":
                            LinkedDocumentCollection res = dc.crawl();
                            DocumentCollectionCell temp = res.getDocuments();
                            while(temp!=null){
                                dc.appendDocument(temp.getInfo());
                                temp = temp.getNext();
                            }
                            break;
                        case "pageRank":
                            double [] PR2 = dc.pageRank(0.85);
                            DocumentCollectionCell temp3 = dc.getDocuments();
                            int p=0;
                            while(temp3!=null){
                                result+=(p+1) + ". " + temp3.getInfo().getTitle() + ";    RageRank = " + PR2[p]+Terminal.NEWLINE;
                                temp3 = temp3.getNext();
                                p++;
                            }
                            break;
                        default:
                            break;
                    }
                    socketWriter.println(result);
                    //socketWriter.print('>');
                    socketWriter.flush();
                }

                client.close();
            }
        } finally {
            serverSocket.close();
        }
    }
}
