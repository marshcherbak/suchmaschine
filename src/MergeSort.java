public class MergeSort {

    public static int[] mergeSortIt(int[] a){
        if(a.length==0)
            return null;
        /* starting from the bottom */
        int [][] temp = new int[a.length][1];

        /* array of arrays consisting of 1 element of a */
        for(int i = 0; i < a.length; i++){
            temp[i][0] = a[i];
        }

        while(temp.length!=1){ // until we have a single array
            // next array or arrays will be twice smaller (or twice smaller + one for the first iteration when a.length is uneven
            int n = temp.length/2 + (temp.length%2);
            int [][] nextMerge = new int[n][];
            int j = 0;
            int i = 0;
            while(i < temp.length){
                if((i+1)==temp.length) // if we have a single last element (can happen in a first iteration)
                    nextMerge[j]=temp[i];
                else { // else merge arrays
                    nextMerge[j] = mergeArrays(temp[i], temp[i + 1]);
                }
                i+=2;
                j++;
            }
            temp = nextMerge;
        }
        return temp[0];
    }

    /* this method merges 2 sorted arrays into a sorted array */
    private static int[] mergeArrays(int [] a, int [] b){
        int [] result = new int[a.length+b.length];
        int i = 0, j = 0;

        int k = 0;
        while(k < result.length){ // until have all elements
            if(i<a.length && j < b.length){ // still have elements in both arrays
                if(a[i]<=b[j]) { //add smaller
                    result[k] = a[i];
                    i++;
                }
                else{
                    result[k] = b[j];
                    j++;
                }
            }
            else if(i < a.length){ // if still have elements in a add them to the result
                result[k] = a[i];
                i++;
            }
            else if(j < b.length){ // if still have elements in b add them to the result
                result[k] = b[j];
                j++;
            }
            k++;
        }
        return result;
    }

    private static void printArray(int [] arr){
        if(arr==null || arr.length == 0) {
            System.out.println("No elements to sort.");
        }
        else {
            for (int i = 0; i < arr.length - 1; i++) {
                System.out.print(arr[i] + ", ");
            }
            System.out.println(arr[arr.length - 1] + ".");
        }
    }
    public static void main(String[] args) {
        int [] a = {1,2,4,5,8};
        int [] b = {-5,4,8,1,158,42,1,0,95,-54,-89,-965,0,1256,784,62,4};
        int [] c = {};
        int [] d = {1};
        int [] e = {100,99,88,77,66,55,44,33,22,11,1,0,-11,-22,-33,-44,-55,-66,-77,-88,-99,-100};
        printArray(mergeSortIt(a));
        printArray(mergeSortIt(b));
        printArray(mergeSortIt(c));
        printArray(mergeSortIt(d));
        printArray(mergeSortIt(e));
    }
}
