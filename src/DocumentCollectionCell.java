public class DocumentCollectionCell {

    private Document info;
    private DocumentCollectionCell next;
    private DocumentCollectionCell prev;
    private double similarity;

    public double getRelevance() {
        return relevance;
    }

    public void setRelevance(double relevance) {
        this.relevance = relevance;
    }

    private double relevance;


    // setter and getter for info
    public Document getInfo() { return info; }
    public void setInfo(Document info) { this.info = info; }

    //setter and getter for next
    public DocumentCollectionCell getNext() { return next; }
    public void setNext(DocumentCollectionCell next) { this.next = next; }

    //setter and getter for prev
    public DocumentCollectionCell getPrev() { return prev; }
    public void setPrev(DocumentCollectionCell prev) { this.prev = prev; }

    //setter and getter for similarity
    public double getSimilarity() { return similarity; }
    public void setSimilarity(double similarity) { this.similarity = similarity; }

    //constructor
    public DocumentCollectionCell(Document info){
        this.info = info;
        this.next = null;
        this.prev = null;
    }

    //additional constructor (with similarity)
    public DocumentCollectionCell(Document info, double similarity){
        this.info = info;
        this.next = null;
        this.prev = null;
        this.similarity = similarity;
    }

    public DocumentCollectionCell(Document info, double similarity, double relevance){
        this.info = info;
        this.next = null;
        this.prev = null;
        this.similarity = similarity;
        this.relevance = relevance;
    }
}
