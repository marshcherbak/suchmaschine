public class CRC {

    int n;
    int poly;

    private int calcDeg(int p){ // calculates degree of any given polynom
        if (p==0)
            return 0;
        if(p < 0)
            return 31;
        int grad = 1;
        int l = 0;
        boolean flag = true;
        while(flag){
            if(grad>p || grad < 0)
                flag = false;
            else {
                grad*=2;
                l++;
            }

        }
        l--;
        return l;
    }
    public CRC(int poly){ //calculates the degree of poly and sets n
        this.poly = poly;
        n = 0;
        n = calcDeg(poly);
    }

    private int getDegree(){
        return n - 1;
    }

    public int crcASCIIString(String s){
        //calculates crc
        //gradually adds parts of the string (as it deletes already used parts), since it can be larger than MAX int value
        if(s.length()==0)
            return poly;
        int data = s.charAt(0); // load first char
        int length = 7;
        int j = 1;
        int seven = (int)Math.pow(2,7);
        int countInt = 0;
        while (length <= n && j!= s.length()){
            data*=seven;
            data += s.charAt(j);
            j++;
            length+=7;
        }
        if(j==s.length()){
            while(length<=n && countInt!=n) {
                data *= 2;
                length += 1;
                countInt++;
            }
        }
        int k, num;
        int m = 0;
        int count = 0;
        while(length!=0 || countInt!= n) {
            if(count == 0) {
                if(calcDeg(data)==n) {
                    m = data;
                    if(j==s.length())
                    length = 0;
                }
                else {
                    int v = length - n - 1;
                    m = data >>> v;
                }
            }
            m = m ^ poly;
            while (calcDeg(m) != calcDeg(poly)&& countInt!=n) {
                boolean m1 = false;
                if(length <= n+1){
                    if(j < s.length()) {
                        data*=seven;
                        data += s.charAt(j);
                        length += 7;
                        j++;
                    }
                   else if (length <= 0){
                        while(calcDeg(m) != calcDeg(poly) && countInt!=n) {
                            m = m << 1;
                            countInt++;
                            count++;
                        }
                        break;
                    }

                    }

                    m *= 2;
                if (count == 0) {
                    num = ones(length - n - 1);
                }
                else
                    num = ones(length);
                k = data;
                k = k & num;
                if (count == 0)
                    length = length - (n + 1) - 1;
                else
                    length = length - 1;
                for (int jj = 0; jj < length; jj++)
                    k /= 2;
                num = ones(length);
                data = data & num;
                m = m + k;
                count++;
            }

        }
        if(calcDeg(m)==calcDeg(poly))
            m = m ^ poly;
        return (int)m;
    }

    private int ones(int num){
        //creates an int that in binary will just have n bits with all of them = 1
        int nd = 0;
        for(int i=0; i < num; ++i){
            int m = (int)Math.pow(2,i);
            nd = nd + m;
        }
        return nd;
    }
}
