/**
 * The class {@code Author} represents an author of a {@see Document} or a
 * {@see Review}.
 * 
 * @author Florian Kelbert
 *
 */
public class Author {
    private String firstName;
    public String getFirstName() {
	  return this.firstName;
  }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    private String lastName;
    public String getLastName() {
	  return this.lastName;
  }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    private Date birthday;
    public Date getBirthday() {
	  return this.birthday;
  }
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
    private String residence;
    public String getResidence() {
	  return this.residence;
  }
    public void setResidence(String residence) {
        this.residence = residence;
    }
    private String email;
    public String getEmail() {
	  return this.email;
  }
    public void setEmail(String email) { //email should contain a @. if not, sets email to the phrase "no email"
        if(email.contains("@"))
            this.email = email;
        else
            this.email = "no email";
    }
    public Author(String firstName, String lastName, Date birthday, String residence, String email) {
        setBirthday(birthday);
        setEmail(email);
        setFirstName(firstName);
        setLastName(lastName);
        setResidence(residence);
  }
  
    public String toString() {
	  return "Author:" + Terminal.NEWLINE + "Name: " + this.firstName + " " + this.lastName + Terminal.NEWLINE + "Birthday: " + 
              this.birthday.toString();
  }
  
    public String getContactInformation() {
	  return this.firstName + " " + this.lastName + Terminal.NEWLINE + this.email + Terminal.NEWLINE + this.residence;
  }
  
    public int getAgeAt(Date today) { //gives age of the author (full years), uses method from class Date. 1 year here is 360 days

      return this.birthday.getAgeInYearsAt(today);
  }

    public boolean equals(Author author){
        if(author!=null) {
            if (this.birthday.equals(author.getBirthday()) && this.email.equals(author.getEmail()) &&
                    this.firstName.equals(author.getFirstName()) && this.lastName.equals(author.getLastName()) &&
                    this.residence.equals(author.getResidence()))
                return true;
        }
        return false;
    }

}
