/**
 * The class {@code Review} represents a review of a {@see Document}.
 * 
 * @author Florian Kelbert
 *
 */
public class Review {
    private String content;

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private Author author;

    public Author getAuthor() {
        return this.author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    private Document revDoc;

    public Document getRevDoc() {
        return this.revDoc;
    }

    public void setRevDoc(Document revDoc) {
        this.revDoc = revDoc;
    }

    private Date releaseDate;

    public Date getReleaseDate() {
        return this.releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    private String language;

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    private int rating;

    public int getRating() {
        return this.rating;
    }

    public void setRating(int rating) { //sets rating if given valid number (1 to 10 inclusive). If not, sets rating to default value -1
        if (rating >= 0 && rating <= 10)
            this.rating = rating;
        else
            this.rating = -1;
    }

    public Review(Author author, Document reviewedDocument, String language,
                  Date releaseDate, int rating, String content) {
        setAuthor(author);
        setContent(content);
        setLanguage(language);
        setRating(rating);
        setReleaseDate(releaseDate);
        setRevDoc(reviewedDocument);
    }

    public String toString() {
        return "Review: " + this.author.toString() + this.revDoc.toString() + Terminal.NEWLINE + "Language: " + this.language +
                this.releaseDate.toString() + Terminal.NEWLINE + "Rating: " + this.rating + Terminal.NEWLINE;
    }

    public int getAgeAt(Date today) { //gives the age of the review in days (month = 30 days, no leap years, year = 360 days), uses method from class Date

        return this.releaseDate.getAgeInDaysAt(today);
    }

    public boolean equals(Review review) {
        if (review != null) {
            if (this.author.equals(review.getAuthor()) && this.rating == review.getRating() &&
                    this.content.equals(review.getContent()) && this.language.equals(review.getLanguage()) &&
                    this.releaseDate.equals(review.getReleaseDate()) && this.revDoc.equals(review.getRevDoc()))
                return true;
        }
        return false;
    }
}

