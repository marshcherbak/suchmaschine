public class WordCount {
    private String word;
    public String getWord() {
        return word;
    }

    private int frequency;
    public int getFrequency() {
        return frequency;
    }
    public void setFrequency(int frequency) { //sets frequency. if given number is < 0, does nothing.
        if(frequency >=0)
        this.frequency = frequency;
    }


    private double weight;
    private double normalizedWeight;
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getNormalizedWeight() {
        return normalizedWeight;
    }

    public void setNormalizedWeight(double normalizedWeight) {
        this.normalizedWeight = normalizedWeight;
    }

    public WordCount() {
        word = "";
        setFrequency(0);
    }

    public WordCount(String word, int frequency){ //constructor, sets values. if given frequency is < 0, sets it to 0.
        if(frequency >= 0)
            this.frequency = frequency;
        else
            this.frequency = 0;
        this.word = word;
    }

    public int incrementCount(){ //increments frequency by 1
        setFrequency(this.frequency+1);
        return getFrequency();

    }
    public int incrementCount(int n){ //increments frequency by n only if n >= 0, if not, does nothing
        if(n>=0)
            setFrequency(this.frequency+n);
        return getFrequency();
    }

    public boolean equals(WordCount wc){
        if(wc != null){
            if(this.frequency == wc.getFrequency() && this.word.equals(wc.getWord()))
                return true;
        }
        return false;
    }
}
