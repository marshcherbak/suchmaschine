public class MatrixVectorOperations {

    public static double[] multiply(double[][] matrix, double[] vector){ //returns the result of matrix * vector
        double [] result = new double[vector.length];

        for(int i = 0; i < matrix.length; i++){
            double temp = 0;
            for(int j = 0; j < vector.length; j++){
                temp+=matrix[i][j]*vector[j];
            }
            result[i] = temp;
        }
        return result;
    }

    public static double cosineSimilarity(double[] v1, double[] v2){ // formula - (a,b)/sqrt((a,a)*(b,b))
        double a = scalarProducr(v1,v2);
        double b = scalarProducr(v1,v1)*scalarProducr(v2,v2);
        double c = Math.sqrt(b);
        return a/c;
    }

    private static double scalarProducr (double[] v1, double [] v2){ // calculates scalar prod of two vectors
        double result = 0;
        for(int i = 0; i < v1.length; i++) {
            result += v1[i]*v2[i];
        }
        return result;
    }

    public static double[][] transpose(double[][] matrix){
        double [][] result = new double[matrix[0].length][matrix.length];
        for(int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++)
                result[j][i] = matrix[i][j];
        }
        return result;
    }

    public static double euclideanDistance(double[] v1, double[] v2){
        double result = 0;
        for(int i = 0; i < v1.length; i++){
            result+=(v1[i]-v2[i])*(v1[i]-v2[i]);
        }
        return Math.sqrt(result);
    }

    /* this method calculates all permutations of numbers 0 to n-1
    * next permutation is calculated from the previous one
    * logic - each permutation is a number, and the result
     * should be an ascending "array" or numbers
     * with each iteration swapping two elements so that we get the next bigger than the previous number*/
    public static int[][] permutations(int n){
        int k = factorial(n); // number of permutations
        int[][] result = new int[k][n];
        int[] start = new int [n]; // starting array and also first permutation
        for(int i = 0; i < n; i++)
            start[i] = i;
        result[0] = start;
        for(int v = 0; v < k-1; v++){
            /* generate next permutation by swapping two elements and then
             * sorting the array from a point where we put a new "leading" element
             * that way we get a smallest possible permutation-number greater than a previous one */
            int max = result[v][n-1]; // last element as max
            int [] newP = new int [n];
            System.arraycopy(result[v],0,newP, 0, n);
            int sMax = newP.length-1;
            for(int i = newP.length-2; i >=0; i--)
                if (newP[i] < max && isDescending(newP, i + 1)) { // if find element less then previous and the rest is already 'done' - here it means sorted descending
                    //swipe elements
                    int temp = newP[i];
                    newP[i] = newP[sMax];
                    newP[sMax] = temp;
                    //sort the rest using selection sort
                    selectionSort(newP, i + 1);
                    break;
                }
                // if got to the first element
                else if (i == 0) {
                    //find new max, set i to its index, which will allow to start searching for the lesser element again
                    for (int j = newP.length - 2; j >= 0; j--) {
                        if (newP[j] > max) {
                            max = newP[j];
                            sMax = j;
                            i = sMax;
                            break;
                        }
                    }
                }
            result[v+1] = newP;
        }
        return result;
    }

    /* this method sorts the array from a certain point using selection sort */
    private static void selectionSort(int [] arr, int ind){
        int counter = ind;

        while(counter < arr.length){ //until all sorted
            int min = arr[counter]; // first - minimal
            int forSwap = counter;
            for(int i = counter; i < arr.length; i++){ //find new minimal
                if(arr[i]< min){
                    min = arr[i];
                    forSwap = i;
                }
            }
            //move new minimal to the sorted part
            int temp = arr[forSwap];
            arr[forSwap] = arr[counter];
            arr[counter]=temp;
            counter++; // increase counter
        }
    }

    /* this method checks if the array is sorted descending from a given index */
    private static boolean isDescending(int [] arr, int ind){
        int i = ind;
        boolean flag = true;
        if(i<0)
            return false;
        while(i < arr.length-1 && flag) {
            if(arr[i+1]>arr[i])
                flag =  false;
            i++;
        }
        return flag;
    }

    /* this method calculates the factorial of a number */
    private static int factorial(int n){
        if(n==0 || n == 1)
            return 1;
        int res = 1;
        for(int i = 1; i <=n; i++){
            res*=i;
        }
        return res;
    }

    /* this method calculates the sign of the permutation */
    public static int sgn(int[] permutation){
        int res1 = 1;
        int res2 = 1;
        for(int j = 0; j < permutation.length; j++) {
            for (int i = 0; i < j; i++) {
                res1 *= (permutation[j] - permutation[i]);
                res2 *= (j - i);
            }
        }
        int res = res1/res2;
        return res;

    }

    /* this method calculates the determinant based on permutations */
    public static int determinant(int[][] A){
        int [][] p = permutations(A.length);
        int det = 0;

        for (int k = 0; k < p.length; k++){
            int t = 1;
            for(int i = 0; i < A.length; ++i){
                t*=A[i][p[k][i]];
            }
            int s = sgn(p[k]);
            s*=t;
            det+=s;
        }

        return det;
    }

    public static void main(String[] args) {
        int n = 1;
        int[][] nn = permutations(n);
        for(int i = 0; i < nn.length; i++)
            for(int j = 0; j < n; j++)
                System.out.println(nn[i][j]);
        int [][] A = {{-2,1,3,2}, {3,0,-1,2}, {-5,2,3,0}, {4,-1,2,-3} };
        int [][] B = {{2,3,0,4,5}, {0,1,0,-1,2}, {3,2,1,0,1}, {0,4,0,-5,0}, {1,1,2,-2,1}};
        System.out.println(determinant(A));
        System.out.println(determinant(B));
    }
}
