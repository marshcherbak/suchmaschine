public class LinkedDocument extends Document {

    private String ID;
    public String getID() { return ID; }

    private LinkedDocumentCollection incomingLinks;
    private LinkedDocumentCollection outgoingLinks;

    private String [] links;
    public String[] getLinks() {
        return links;
    }

    public LinkedDocumentCollection getOutgoingLinks(){
        if(outgoingLinks == null)
            createOutgoingDocumentCollection();
        return outgoingLinks;
    }

    /**
     * This private helper method uses the {@link String}-Array of IDs. It creates a
     * new {@link LinkedDocument} for every ID and adds it to the
     * {@link LinkedDocumentCollection} of outgoing links, if it is not a self-link.
     */
    private void createOutgoingDocumentCollection(LinkedDocumentCollection cache) {
        this.outgoingLinks = new LinkedDocumentCollection();

        LinkedDocument newDoc;
        for (int i = 0; i < this.links.length; i++) {
            newDoc = cache.findByID(this.links[i]);

            if (newDoc == null)
                newDoc = LinkedDocument.createLinkedDocumentFromFile(this.links[i]);

            /* do not add links to this page (page pointing to itself) */
            if (!this.equals(newDoc)) {
                this.outgoingLinks.appendDocument(newDoc);
            }
        }
    }

    /**
     * This method returns a {@link LinkedDocumentCollection} that contains all
     * {@link LinkedDocument}s that this instance links to.
     *
     * @return a {@link LinkedDocumentCollection} containing all
     *         {@link LinkedDocument} this instance links to
     */
    public LinkedDocumentCollection getOutgoingLinks(LinkedDocumentCollection cache) {
        if (this.outgoingLinks == null) {
            this.createOutgoingDocumentCollection(cache);
        }

        return this.outgoingLinks;
    }

    public LinkedDocumentCollection getIncomingLinks(){
        return incomingLinks;
    }

    //constructor
    public LinkedDocument(String title, String language, String summary,
                          Date releaseDate, Author author, String content, String id){
        super(title,language,summary,releaseDate,author,content);
        this.ID = id;
        links = findOutgoingIDs(content);
        setLinkCountZero();
    }

    @Override
    /* if doc is a LinkedDoc returns is IDs are equal
    * else called equals method of class documents */
    public boolean equals(Document doc){
        if(doc instanceof LinkedDocument){
            return this.ID.equals(((LinkedDocument) doc).ID);
        }
        else
            return super.equals(doc);
    }

    /**
     *  finds all outgoing IDs in text and saves them in links[]
    * returns links
    * if there are no links in the text returns an empty array
    * if text is null returns null
    * if text is empty returns null */
    private String[] findOutgoingIDs(String text){
        if(text == null || text.length()==0)
            return null;
        //use Stringulina to find how many links text has
        int n = Stringulina.countSubstring(text, "link:");
        String [] links = new String[n];
        int i = 0;
        while(i < n){
            int ind = text.indexOf("link:");
            text = text.substring(ind+5);
            int ind2 = text.indexOf(' ');
            String s;
            if(ind2==-1)
                s = text;
            else
                s = text.substring(0, ind2);
            links[i] = s;
            text = text.substring(ind2+1);
            i++;
        }
        return links;
    }

    /* sets all words with "link:" count 0 */
    private void setLinkCountZero(){
        for(int i = 0; i < getWordCounts().size(); i++){
            if(getWordCounts().getWord(i).contains("link:"))
                getWordCounts().setCount(i, 0);
        }
    }


    /* adds document to incominglinks collection */
    public void addIncomingLink(LinkedDocument incomingLink){
        if(incomingLinks==null)
            incomingLinks = new LinkedDocumentCollection();
        if(!this.equals(incomingLink))
            incomingLinks.appendDocument(incomingLink);
    }

    /**
     *  creates a LinkedDocument from a file
    * where filename is document's id
    * first line is the title of the document
    * second line is the text of the document */
    public static LinkedDocument createLinkedDocumentFromFile(String fileName){
        String forReading = fileName;
        String [] str = Terminal.readFile(fileName);
        if(str==null || str.length<2)
            return null;
        LinkedDocument LD = new LinkedDocument(str[0], "de", "summary", new Date(),
                new Author("FN", "LN", new Date(), "res", "e@mail"),
                str[1], fileName);
        return LD;
    }

    /**
     *  creates collection of all documents this document links to
    * documents have to be files */
    private void createOutgoingDocumentCollection(){
        outgoingLinks = new LinkedDocumentCollection();
        boolean self;
        for(int i = 0; i < links.length; i++){
            self = false;
            if(!links[i].equals(this.ID)) {
                LinkedDocument ld = createLinkedDocumentFromFile(links[i]);
                outgoingLinks.appendDocument(ld);
            }
        }
    }
}
