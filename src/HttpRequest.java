import java.util.*;
import java.io.*;
import java.util.stream.*;

public class HttpRequest {
    private String request;

    public String getRequest() {
        return request;
    }

    public HttpRequest(String request) {
        this.request = request;

        try {
            ArrayList<String> processing = elements(request);

            //method
            String m = processing.get(0);
            if(m.equals("GET"))
                method = HttpMethod.GET;
            else if(m.equals("POST"))
                method = HttpMethod.POST;
            else
                throw  new RuntimeException();

            //path
            String [] temp = processing.get(1).split("\\?");
            processing = new ArrayList<>(Arrays.asList(temp));
            path = processing.get(0);
            paramenters = new HashMap<String, String>();
            if(!(processing.size()==1)) {
                temp = processing.get(1).split("=");
                processing = new ArrayList<>(Arrays.asList(temp));
                paramenters.put(processing.get(0), processing.get(1));
            }

        }
        catch(Exception e){

        }
    }

    private HttpMethod method;

    public HttpMethod getMethod() {
        return method;
    }

    private String path;

    public String getPath() {
        return path;
    }

    public Map<String, String> getParamenters() {
        return paramenters;
    }

    private Map<String, String> paramenters;

    private ArrayList<String> elements(String str){
        String[] splitted = str.split(" ");
        ArrayList<String> res = new ArrayList<>(Arrays.asList(splitted));
        return res;
    }

    public static void main(String[] args) {
        HttpRequest r = new HttpRequest("GET /search?query=einfach HTTP/1.1");
    }
}
