import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.stream.Stream;

public class WebServer implements Runnable{

    private static LinkedDocumentCollection ldc;
    private static HttpResponse handleMainPage(){
        TemplateProcessor tp = new TemplateProcessor("search.html");
        java.util.TreeMap<String, String> varass = new java.util.TreeMap<>();
        varass.put("%searchvalue", "");
        varass.put("%table", "");
        String text = tp.replace(varass);
        return new HttpResponse(HttpStatus.Ok, text);
    }

    private static HttpResponse handleSearchRequest(String query){
        ldc.match(query);
        DocumentCollectionCell temp3 = ldc.getDocuments();
        int l = 1;
        String res = "";
        while(temp3!=null){
            res += "<tr><td>" + l + "</td><td>";
            res+= "<a href=\"" + temp3.getInfo().getTitle() + "\">" + temp3.getInfo().getTitle() + "</a></td>";
            res+= "<td>"+ ldc.getRelevance(l-1)+"</td></tr>";
            temp3 = temp3.getNext();
            l++;
        }

        TemplateProcessor tp = new TemplateProcessor("search.html");
        java.util.TreeMap<String, String> varass = new java.util.TreeMap<>();
        varass.put("%searchvalue", "value="+query);
        varass.put("%table", res);
        String text = tp.replace(varass);
        return new HttpResponse(HttpStatus.Ok, text);
    }

    private static HttpResponse handleFileRequest(String fileName){
        if(fileName.equals("favicon.ico"))
            return handleMainPage();
        else {
            String[] str = Terminal.readFile(fileName);
            String res = "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>PGdP Search Engine</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<center>\n" +
                    "    <h2>PGdP Search Engine</h2>\n" +
                    "    <form action=\"/search\">\n" +
                    "    <input type=\"text\" name=\"query\" %searchvalue>\n" +
                    "    <input type=\"submit\" value=\"Submit Query\">\n" +
                    "    </form>\n" +
                    "    </center>\n" +
                    "\n" +
                    "<br><br>\n" +
                    "\n" +
                    str[1] +
                    "</body>\n" +
                    "</html>";
            return new HttpResponse(HttpStatus.Ok, res);
        }
    }

   // private ServerSocket server;
    private Socket socket;
    String response;
    public WebServer(Socket socket) throws IOException {
        this.socket = socket;
       // server = new ServerSocket(port);
        double pageRankDampingFactor = 0.85;
        double weightingFactor = 0.6;

        {
            LinkedDocumentCollection temp = new LinkedDocumentCollection();
            temp.appendDocument(new LinkedDocument("b.txt", "", "", null, null,
                    "link:A.txt link:E.txt", "b.txt"));
            ldc = temp.crawl();
        }
        run();
    }


    public void run(){
        try{
            //socket = server.accept();
            PrintStream out = new PrintStream(socket.getOutputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String s = br.readLine();
            HttpRequest hr = new HttpRequest(s);

            if (hr.getPath().equals("/")) {
                HttpResponse hresp = handleMainPage();
                response = hresp.toString();
            } else if (hr.getPath().equals("/search")) {
                HttpResponse hresp = handleSearchRequest(hr.getParamenters().get("query"));
                response = hresp.toString();
            }
            else {
                String fName = hr.getPath().substring(1);
                HttpResponse hresp = handleFileRequest(fName);
                response = hresp.toString();
            }
            out.print(response);
            out.flush();
        }
        catch(Exception e){

        }finally {
            if(socket!=null)
                try{
                    socket.close();
                } catch(IOException e){

                }
        }
    }



    public static void main(String[] args) throws IOException{
        ServerSocket server = new ServerSocket(8000);
        while(true){
            Socket s = server.accept();
            new Thread(new WebServer(s)).start();
        }

    }
}
