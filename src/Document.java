/**
 * The class {@code Document} represents a document.
 * 
 * @author Florian Kelbert
 *
 */
public class Document {
    public static final String[] SUFFICES = { "ab", "al", "ant", "artig", "bar", "chen", "ei", "eln", "en", "end", "ent",
			"er", "fach", "fikation", "fizieren", "faehig", "gemaeß", "gerecht", "haft", "haltig", "heit", "ie", "ieren", "ig", "in",
			"ion", "iren", "isch", "isieren", "isierung", "ismus", "ist", "itaet", "iv", "keit", "kunde", "legen", "lein",
			"lich", "ling", "logie", "los", "mal", "meter", "mut", "nis", "or", "sam", "schaft", "tum", "ung", "voll", "wert",
			"wuerdig" };



    private WordCountsArray wordCounts;
    public WordCountsArray getWordCounts() {
        return wordCounts;
    }

	private String title;
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private String language;
	public String getLanguage() {
		return this.language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}

	private String summary;
	public String getSummary() {
		return this.summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}

	private Date releaseDate;
	public Date getReleaseDate() {
		return this.releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	private Author author;
	public Author getAuthor() {
		return this.author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}

	public Document(String title, String language, String summary,
					Date releaseDate, Author author, String content) {
		setAuthor(author);
		addContent(content);
		setLanguage(language);
		setReleaseDate(releaseDate);
		setSummary(summary);
		setTitle(title);
	}

	public String toString() {
		return "Title: " + this.title + " Author:" + this.author.toString();
	}
	
	public int getAgeAt(Date today) { //gives age of the document in days (month = 30 days, no leap years, year = 360 days), uses method from class Date
		return this.releaseDate.getAgeInDaysAt(today);
	}

    private static String[] tokenize(String content){ //parses String content into an array of strings-words
        //if content is empty, returns array with 1 empty string as an element
        //if content is null, returns null
        if(content == null)
            return null;
        int n = Stringulina.countSubstring(content, " ");
        String [] tokens = new String[n+1];
        for(int i = 0; i < n+1; ++i)
            tokens[i] = "";
        int k = 0;
        for(int i = 0; i < content.length(); i++){
            if(content.charAt(i)!= ' ')
                tokens[k]+=content.charAt(i);
            else
                k++;
        }
        return tokens;
    }

    private static boolean sufficesEqual(String word1, String word2, int n) {
        /* if n is too large, last n chars are not equal */
        if (n > word1.length() || n > word2.length()) {
            return false;
        }

        boolean isEqual = true;
        int i = 0;

        while (isEqual && i < n) {
            /* begin comparison at last char */
            if (word1.charAt(word1.length() - 1 - i) != word2.charAt(word2.length() - 1 - i)) {
                isEqual = false;
            }
            i++;
        }

        return isEqual;
    }
    // from masterloesung
    private static String findSuffix(String word) {
        if (word == null || word.equals("")) {
            return null;
        }

        String suffix = "";
        String suffixHit = "";
        int i = 0;

        while (i < Document.SUFFICES.length) {
            suffix = Document.SUFFICES[i];

            /* check, if this suffix is a suffix of word */
            if (sufficesEqual(word, suffix, suffix.length())) {
                if (suffixHit.length() < suffix.length()) {
                    suffixHit = suffix;
                }
            }
            i++;
        }
        return suffixHit;
    }

    //from masterlosung
    private static String cutSuffix(String word, String suffix) {
        if (suffix == null || suffix.equals("")) {
            return word;
        }

        if (word == null) {
            return null;
        }

        /* not a suffix */
        if (!sufficesEqual(word, suffix, suffix.length())) {
            return word;
        }

        /* create word without suffix, by copying all characters of the word stem */
        String wordWithoutSuffix = "";

        for (int i = 0; i < word.length() - suffix.length(); i++) {
            wordWithoutSuffix = wordWithoutSuffix + word.charAt(i);
        }

        return wordWithoutSuffix;
    }

    private void addContent(String content){
        //parses content into words, cuts off suffixes from them, and
        //adds all the words (already wicth cut-off suffixes) to wordCounts
        //words can repeat, each has frequency 1
        String [] tokens = tokenize(content);
        wordCounts = new WordCountsArray(tokens.length+1);
        for(int i = 0; i < tokens.length; i++){
            String suffix = findSuffix(tokens[i]);
            String s = cutSuffix(tokens[i], suffix);
            wordCounts.add(s,1);
        }
    }

    public boolean equals(Document document){
	    if(document!= null){
	        if(this.title.equals(document.getTitle()) && this.author.equals(document.getAuthor()) &&
            this.summary.equals(document.getSummary()) && this.releaseDate.equals(document.getReleaseDate()) &&
            this.language.equals(document.getLanguage()) && this.wordCounts.equals(document.getWordCounts()))
	            return true;
        }
        return false;
    }


    public static void main(String[] args) {
		String [] t = tokenize("Sie für diese Aufgabe davon aus, dass der Text text ausschließlich Kleinbuchstaben " +
				"und Leerzeichen enthält. Zwei Wörter sind dabei jeweils durch genau ein Leerzeichen " +
				"voneinander getrennt. Die Anzahl der Wörter im übergebenen Text ergibt sich aus " +
				"der Anzahl der Leerzeichen plus 1.");

		sufficesEqual("Fahigjkeit", "Leerekeit", 4);
		sufficesEqual("helooo", "helool", 2);

		String s = cutSuffix("Fahigkeit", "keit");
		Document d = new Document("hello", "en", "jjjj", new Date(),
                new Author("l", "m", new Date(), "", ""), "Sie für diese Aufgabe davon aus dass der Text text ausschließlich Kleinbuchstaben " +
                "und Leerzeichen enthält Zwei Wörter sind dabei jeweils durch genau ein Leerzeichen " +
                "voneinander getrennt Die Anzahl der Wörter im übergebenen Text ergibt sich aus " +
                "der Anzahl der Leerzeichen plus 1");


	}
}
