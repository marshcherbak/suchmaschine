public class StringulinaTest {
    private static void sayifSubstringandPos(String haystack, String needle){ //say if needle is in haystack, and if yes, the position
        String yesOrNo;
        String position = "";
        int i = Stringulina.substringPos(haystack, needle);
        if(i!=-1){
            yesOrNo = " is in the string ";
            position = " Position: " + i;
        }
        else
            yesOrNo = " is not in the string ";
        System.out.println("String " + "\"" + needle + "\"" + yesOrNo + "\"" + haystack + "\"" + ". " + position);

    }
    private static void sayHowMany(String haystack, String needle){ //says how many instances of needle are in the haystack
        System.out.println("There are " + Stringulina.countSubstring(haystack, needle) + " instances of the string " + "\"" + needle + "\"" + " in the string "+ "\"" + haystack + "\"");

    }
    private static void sayBrackets(String str){ //says if the word is correctly bracketed
        String yesOrNo;
        if(Stringulina.correctlyBracketed(str))
            yesOrNo = " is correctly bracketed.";
        else
            yesOrNo = " is not correctly bracketed.";
        System.out.println("String " + "\"" + str + "\"" + yesOrNo);
    }
    private static void sayMatches(String str, String pattern){ // says if string str matches the pattern
        String yesOrNo;
        if(Stringulina.matches(str, pattern))
            yesOrNo = " matches pattern ";
        else
            yesOrNo = " doesn't match pattern ";
        System.out.println("String " + "\"" + str + "\"" + yesOrNo + pattern);
    }
    public static void main(String[] args) { //running all tests for different values
        //checks all possible varians of bracketing to see if it works properly
        sayBrackets("");
        sayBrackets(")(");
        sayBrackets("kvbkbvkdjf");
        sayBrackets("(hvdkv)((vibbf)kjbv)");
        sayBrackets("a(xx(]))");
        sayBrackets("a(xx))");
        sayBrackets("a(xx)(");
        sayBrackets("a)xx()(");
        System.out.println();

        String s1 = "Oy with the poodles already!";
        String s2 = "“I live in two worlds, one is a world of books.” (Rory)";
        String p = "poodles";
        String w = "world";
        String a4 = "aaaaa";
        String a = "aa";
        //checks when string needle is in haystack, same as haystack, and is not in haystack
        sayifSubstringandPos(s1,p);
        sayifSubstringandPos(s1,s1);
        sayifSubstringandPos(p, s1);
        sayifSubstringandPos(s2,w);
        sayifSubstringandPos(s1,a);
        System.out.println();
        //checks for cases when needle is 0, 1, or more times in the haystack
        sayHowMany(s1,p);
        sayHowMany(s2,w);
        sayHowMany(s1,a);
        sayHowMany(a4, a);
        System.out.println();
        //checks matches method for when words are matching and when not
        //if results are right, all the private methods used for "matches" work correctly as well.
        sayMatches("Pijnguin","P.{2}ngui{1}."); //should match
        sayMatches("Haaaaaaaaaawko","Ha{10}..o"); //should match
        sayMatches("Haaawko","Ha{10}..o"); // don't match
        sayMatches("Oy with the poodles already!", "Oy {1}w{1}ith the po{2}dk{0}les already!"); //case where {0}

    }
}
