public class MatricesStudTest {

    public static void printVector(double [] v){
        for(int i = 0; i < v.length; i++)
            System.out.println(v[i]);
    }
    public static void printMatrix(double [][] A){
        for(int i = 0; i < A.length; i++){
            for(int j = 0; j < A[0].length; j++)
                System.out.print(A[i][j] + " ");
            System.out.println();
        }
    }

    public static void testMultiply(double [][] A, double [] b){
        System.out.println("Multiplication: ");
        printMatrix(A);
        System.out.println("*");
        printVector(b);
        System.out.println("=");
        printVector(MatrixVectorOperations.multiply(A,b));
    }

    public static void testCosineSimilarity(double[] v1, double [] v2){
        System.out.println("Cosine similarity: ");
        printVector(v1);
        System.out.println("and");
        printVector(v2);
        System.out.println("= " + MatrixVectorOperations.cosineSimilarity(v1,v2));
    }

    public static void testTranspose(double [][] A){
        System.out.println("Transpose:");
        System.out.println("Original matrix:");
        printMatrix(A);
        System.out.println("Transposed matrix: ");
        printMatrix(MatrixVectorOperations.transpose(A));
    }

    public static void testEuclideanDistance(double [] v1, double [] v2){
        System.out.println("Euclidean distance: ");
        printVector(v1);
        System.out.println("and");
        printVector(v2);
        System.out.println("=" + MatrixVectorOperations.euclideanDistance(v1,v2));
    }

    public static void printIntVector(int [] v){
        for(int i = 0; i < v.length; i++){
            System.out.print(v[i]+ " ");
        }
        System.out.println();
    }
    public static void testPermutations(int n){
        int[][] p = MatrixVectorOperations.permutations(n);
        System.out.println("There are " + p.length + " permutations of numbers from 0 to " + (n-1) + ":");
        for(int i = 0; i < p.length; i++) {
            System.out.println("Permutation " + (i+1) + ":");
            printIntVector(p[i]);
        }
    }

    public static void testSgn(int [] p){
        System.out.print("Sign of permutation ");
        printIntVector(p);
        System.out.println("= " + MatrixVectorOperations.sgn(p));
    }

    public static void testDeterminant(int [][] A){
        System.out.println("Determinant of ");
        for(int i = 0; i < A.length; i++)
            printIntVector(A[i]);
        System.out.println(" = " + MatrixVectorOperations.determinant(A));
    }
    public static void main(String[] args) {
        double [][] A = {{-2,1,3,2}, {3,0,-1,2}, {-5,2,3,0}, {4,-1,2,-3} };
        double [][] B = {{2,3,0,4,5}, {0,1,0,-1,2}, {3,2,1,0,1}, {0,4,0,-5,0}, {1,1,2,-2,1}};

        int [][] AI = {{-2,1,3,2}, {3,0,-1,2}, {-5,2,3,0}, {4,-1,2,-3} };
        int [][] BI = {{2,3,0,4,5}, {0,1,0,-1,2}, {3,2,1,0,1}, {0,4,0,-5,0}, {1,1,2,-2,1}};
        double [] a = {1,2,3,4,5};
        double [] b = {1,3,4,2};
        double [] c = {1,11,1,2,2};

        int n1 = 10;
        int n2 = 5;


        //testPermutations(n1); - takes quite some time to print out all permutations, uncomment if needed
        System.out.println();
        testPermutations(n2);

        System.out.println();

        testMultiply(A,b);
        testMultiply(B,a);

        testCosineSimilarity(a,c);
        testEuclideanDistance(a,c);
        testTranspose(A);
        testTranspose(B);

        System.out.println();

        testSgn(MatrixVectorOperations.permutations(n1)[35488]);
        testSgn(MatrixVectorOperations.permutations(n2)[25]);

        testDeterminant(AI);
        testDeterminant(BI);
    }
}
