public class HttpResponse {
    private HttpStatus status;
    private String body;

    public HttpStatus getStatus() {
        return status;
    }

    public String getBody() {
        return body;
    }

    public HttpResponse(HttpStatus status, String body) {
        this.status = status;
        this.body = body;
    }

    public String toString(){
        return "HTTP/1.1 " + status.getCode() + " " + status.getText() + "\r\n"+
                "content-type: text/html; charset=UTF-8\r\n\r\n" + body;
    }
}
