public class WordCountTest {

    private static void testIncrementOne(WordCount w){
        System.out.println(w.getWord() + ": old frequency = " + w.getFrequency());
        System.out.println("Incrementing frequency by 1.");
        System.out.println("New frequency = " + w.incrementCount());
    }
    private static void testIncrement(WordCount w, int n) {
        System.out.println(w.getWord() + ": old frequency = " + w.getFrequency());
        System.out.println("Trying to increment frequency by " + n);
        System.out.println("New frequency = " + w.incrementCount(n));
    }
    private static void testSetter(WordCount w, int n){
        System.out.println(w.getWord() + ": old frequency = " + w.getFrequency());
        System.out.println("Trying to set frequency " + n);
        w.setFrequency(n);
        System.out.println("New frequency = " + w.getFrequency());
    }
    public static void main(String[] args) {

        WordCount w = new WordCount("pinguins", 777);
        WordCount w0 = new WordCount("hello", -5);

        System.out.println(w0.getWord() + " " + w0.getFrequency()); // check to see that constructor sets frequoncy to 0 when given a negative value

        testIncrementOne(w); //check if increment works

        testIncrement(w,10); //check if increment by n works
        testIncrement(w, -7); //check for correct action of the method when given a negative number

        testSetter(w,5); //check setter
        testSetter(w, -1); //check setter for when given a negative number

    }
}
