public class LinkedDocumentCollection extends DocumentCollection {

    private int[][] indexes; //indexes of docs needed for recursive page rank
    private int [] sumC; //for recursive page rank
    private int n; //number of documents

    //add document to the start of the list if it's of type LinkedDocument
    public void prependDocument(Document doc){
        if(doc instanceof LinkedDocument) {
            if (!contains(doc)) {
                super.prependDocument((LinkedDocument)doc);
            }
        }
    }

    //add document to the end of the list if it's of type LinkedDocument
    public void appendDocument(Document doc){
        if(doc instanceof LinkedDocument) {
            if (!contains(doc)) {
                super.appendDocument((LinkedDocument)doc);
            }
        }
    }

    /* this method adds incoming links too all the documents in the collection */
    public void calculateIncomingLinks(){
        DocumentCollectionCell check = getDocuments();

        while(check!=null){
            LinkedDocument ld = (LinkedDocument)check.getInfo(); //get one document
            DocumentCollectionCell go = getDocuments();
            while(go!=null){ //
                LinkedDocument ld2 = (LinkedDocument)go.getInfo();
                if(!ld2.equals(ld)) { //get incoming links for ld from all documents except ld itself
                    DocumentCollectionCell temp = ld2.getOutgoingLinks().getDocuments();
                    while (temp!=null) {
                        if (temp.getInfo().equals(ld)) {
                            ld.addIncomingLink(ld2); // if found a link to ld add it and break cycle
                            temp=null;
                        }
                        if(temp!=null) {
                            temp = temp.getNext();
                        }
                    }
                }
                go = go.getNext();
            }
            check = check.getNext();
        }
    }
    /**
     * Private helper method that crawls this collection.
     *
     * This method adds all LinkedDocuments of this collection to the specified
     * {@link LinkedDocumentCollection}, if they are not already contained.
     * Additionally, this method recursively crawls all outgoing links of all
     * {@link LinkedDocument}s of this collection.
     *
     * @param resultCollection in and out parameter. All LinkedDocuments are
     *                         (recursively) added to this
     *                         {@link LinkedDocumentCollection}.
     */
    private void crawl(LinkedDocumentCollection resultCollection) {
        if (this.numDocuments() == 0) {
            return;
        }

        /*
         * loop over all documents of this collection and add them to the in/out
         * parameter, if not already contained.
         */
        for (int i = 0; i < this.numDocuments(); i++) {
            LinkedDocument doc = (LinkedDocument) this.get(i);

            if (!resultCollection.contains(doc)) {
                resultCollection.appendDocument(doc);

                /* do the same recursively */
                doc.getOutgoingLinks(resultCollection).crawl(resultCollection);
            }
        }
    }

    /**
     * This method crawls this {@link LinkedDocumentCollection} and returns a new
     * {@link LinkedDocumentCollection}.
     *
     * The returned LinkedDocumentCollection contains all LinkedDocuments of this
     * LinkedDocumentCollection plus any LinkedDocuments that the LinkedDocuments of
     * this collection link to. If these additional LinkedDocuments again link to
     * other LinkedDocuments they will be included as well, and so on.
     *
     * @return a {@link LinkedDocumentCollection} that contains all
     *         {@link LinkedDocument}s of this collection plus any
     *         {@link LinkedDocument}s that are linked either directly or
     *         indirectly.
     */
    public LinkedDocumentCollection crawl() {
        /* prepare the resulting collection and begin crawling ... */
        LinkedDocumentCollection resultCollection = new LinkedDocumentCollection();
        this.crawl(resultCollection);
        return resultCollection;
    }

    /**
     * Finds a {@link LinkedDocument} with the given id
     * if contained in this {@link LinkedDocumentCollection}
     *
     * @param id           the id to be looked up
     * @return the found {@link LinkedDocument} or null
     */
    public LinkedDocument findByID(String id) {
        for (int i = 0; i < this.numDocuments(); i++)
            if (((LinkedDocument) this.get(i)).getID().equals(id))
                return ((LinkedDocument) this.get(i));
        return null;
    }

    /**
     * calculates set of indexes of docs that link to the document at index j*/
    private int [] calcSet(int[][] C, int j){
        int m = 0;
        for(int k = 0; k < n; k++)
            m+=C[j][k];
        int [] res = new int[m];
        int k = 0;
        for(int i = 0; i < numDocuments(); i++){
            if(C[j][i]==1){
                res[k] = i;
                k++;
            }
        }
        return res;
    }

    /**
     * calculates sum of C[k][j] with j given and k from 0 to n-1
     * doesn't check for mistakes as it's always called with a valid j */
    private int sumC(int[][] C, int j){
        int n = numDocuments();
        int sum = 0;
        for(int k = 0; k < n; k++)
            sum+=C[k][j];
        return sum;
    }

    /**
     * recursive PageRank
     * uses already pre-calculated indexes and sums as they don't change
     * calculates pageRank for document with index i
     * uses recDepth for stopping */
    private double pageRankRec(int[][] C, int i, double d, int recDepth){
        double sum = 0;
        if(recDepth > 0) {
            for (int j = 0; j < indexes[i].length; j++) {
                int k = indexes[i][j];
                sum+=pageRankRec(C, k, d, recDepth-1)/sumC[k];
            }
            return (1.-d)/n + d*sum;
        }
        return 1/n;
    }

    /**
     * calculates pageranc for each document in the collection
     * for that calculates indexes and sumxC
     */
    public double[] pageRankRec(double d){
        calculateIncomingLinks();
        n = numDocuments();
        int[][] C = calculateC();
        //initial weights, all equal 1/n, where n is the number of documents
        //prFinal = new double[numDocuments()];
        double[] pr = new double[n];
        sumC = new int[n];
        indexes = new int [n][];

        for(int j = 0; j < n; j++){
            indexes[j] = calcSet(C, j);
            sumC[j] = sumC(C,j);
        }
        for(int i = 0; i < numDocuments(); i++){
            /**
             * here 55 since a bigger value will take longer than a minute :(
             * with this number for some cases the accuracy is worse than 10^-6
             * recDepth should be 100 in order to reach needed accuracy */
            pr[i] = pageRankRec(C, i, d, 55);
        }
        return pr;
    }

    /**
     * calculates matrix C and returns it
     * C[j][i] = 1 if document j links to doc i
     * 0 if not
     * if some document has no outgoing links it'll link to all other documents except itself*/
    private int[][] calculateC(){
        int[][] res = new int[numDocuments()][numDocuments()];
        for(int i = 0; i < numDocuments(); i++)
            for(int j  = 0; j < numDocuments(); j++){
                LinkedDocument d = (LinkedDocument)get(i);
                if(d.getOutgoingLinks().isEmpty() && j!=i)
                    res[j][i] = 1;
                else if(d.getOutgoingLinks().contains((LinkedDocument)get(j)))
                    res[j][i] = 1;
                else
                    res[j][i] = 0;
            }
        return res;
    }

    /*iterative PageRank*/

    /* calculates initial matrix A*/
    private double[][] calcAForIter(){
        calculateIncomingLinks();
        double[][] res = new double[numDocuments()][numDocuments()];
        for(int i = 0; i < numDocuments(); i++)
            for(int j  = 0; j < numDocuments(); j++){
                LinkedDocument d = (LinkedDocument)get(i);
                if(d.getOutgoingLinks().isEmpty())
                    res[j][i] = 1./numDocuments();
                else if(d.getOutgoingLinks().contains((LinkedDocument)get(j)))
                    res[j][i] = 1.0/d.getOutgoingLinks().numDocuments();
                else
                    res[j][i] = 0;
            }
        return res;
    }

    /*matrix vector multiplication*/
    private static double[] multiply(double[][] matrix, double[] vector){
        double [] result = new double[vector.length];

        for(int i = 0; i < matrix.length; i++){
            double temp = 0;
            for(int j = 0; j < vector.length; j++){
                temp+=matrix[i][j]*vector[j];
            }
            result[i] = temp;
        }
        return result;
    }

    /* iterative pageRank */
    public double[] pageRank(double dampingFactor){
        int n = numDocuments();
        double [][] A = calcAForIter(); //matrix A

        double [] V = new double[n];
        for(int i = 0; i < V.length; i++) //initial rank vector
            V[i] = 1./n;

        double [][] M = new double[n][n]; //matrix M calculated accordingly to the formula

        for(int i = 0; i < M.length; i++){
            for(int j = 0; j < M.length; j++){
                M[i][j] = dampingFactor*A[i][j]+(1-dampingFactor)/n;
            }
        }
        double [] checkingV = new double[V.length];
        double [] newV = checkingV;
        double[][] newM = M;
        for(int i = 0; i < V.length; i++)
            checkingV[i]=V[i];
        do{ //
            checkingV = newV;
            newM = matrixXmatrix(newM, M);
            newV = multiply(newM, V);
        }while (!checkErr(newV, checkingV));
        return newV;
    }

    /**
     * check if approximated t the real ranks with 10^-6 accuracy */
    private boolean checkErr(double[] a, double[] b){
        boolean yes = true;
        int i = 0;
        double er = Math.pow(10, -7);
        while(i < a.length && yes){
            if(Math.abs(a[i]-b[i]) >= er)
                yes = false;
            i++;
        }
        return yes;
    }

    /**
     * matrix * matrix
    * no checking for mistakes since it'll always get two square matrices */
    private double[][] matrixXmatrix(double [][] matrix1, double [][] matrix2){
        double[][] res = new double[matrix1.length][matrix1.length];
        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res.length; j++) {
                for (int k = 0; k < res.length; k++) {
                    res[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        return res;
    }

    /**
     * calculates relevance based on the formula
     * sorts documents in the collection by relevance after calculatig pageRank (in case pageRank wasn't called beforehand)
     * returns double [] relevance*/
    private double[] sortByRelevance(double dampingFactor, double weightingFactor) {
        double[] PR = pageRank(dampingFactor);
        n= numDocuments();
        double [] relevance = new double[n];
        DocumentCollectionCell temp = getDocuments();
        for (int i = 0; i < n; i++) {
            relevance[i] = weightingFactor * this.getQuerySimilarity(i) + (1 - weightingFactor) * PR[i];
            temp.setRelevance(relevance[i]);
            temp = temp.getNext();
        }

        sortByRelevance();
        DocumentCollectionCell temp2 = getDocuments();
        for (int i = 0; i < n; i++) {
            relevance[i] = getRelevance(i);
            temp2 = temp2.getNext();
        }
        return relevance;
    }

    /* get relevance of the doc at index */
    public double getRelevance(int index){
        if(isEmpty() || index >= numDocuments() || index < 0)
            return -1;
        else {
            DocumentCollectionCell temp = getDocuments();
            int i = 0;
            while(i < index){
                temp = temp.getNext();
                i++;
            }
            return temp.getRelevance();
        }
    }


    //sorts documents by relevance in the collection using iterative merge sort
    private void sortByRelevance() {
        LinkedDocumentCollection [] tempc = new LinkedDocumentCollection[this.numDocuments()]; //array of collections

        for(int i = 0; i < this.numDocuments(); i++){ //array of collection, each consisting of a single document
            tempc[i] = new LinkedDocumentCollection();
            tempc[i].appendDocWithRelevance((LinkedDocument)this.get(i), this.getQuerySimilarity(i), this.getRelevance(i));
        }

        while(tempc.length!=1){
            int n2 = tempc.length/2 + (tempc.length%2);
            LinkedDocumentCollection [] nextMerge = new LinkedDocumentCollection[n2];
            int j = 0;
            int i = 0;
            while(i < tempc.length){
                if((i+1)==tempc.length)
                    nextMerge[j]= tempc[i];
                else {
                    nextMerge[j] = mergeLists(tempc[i], tempc[i + 1]);
                }
                i+=2;
                j++;
            }
            tempc = nextMerge;
        }
        this.setDocuments(tempc[0].getDocuments());
    }

    /* this method merges two sorted collections into one (sorted) based on relevance */
    private LinkedDocumentCollection mergeLists(LinkedDocumentCollection a, LinkedDocumentCollection b){
        LinkedDocumentCollection result = new LinkedDocumentCollection();
        int num = a.numDocuments()+b.numDocuments();
        int i = 0;
        int j = 0;
        while(result.numDocuments()!=num){
            if(i!=a.numDocuments() && j!=b.numDocuments()){
                if(a.getRelevance(i)>=b.getRelevance(j)){ //add bigger element to resulting collection
                    result.appendDocWithRelevance((LinkedDocument)a.get(i), a.getQuerySimilarity(i), a.getRelevance(i));
                    i++;
                }
                else{
                    result.appendDocWithRelevance((LinkedDocument)b.get(j), b.getQuerySimilarity(j), b.getRelevance(j));
                    j++;
                }
            }
            else if (i<a.numDocuments()){
                result.appendDocWithRelevance((LinkedDocument) a.get(i), a.getQuerySimilarity(i), a.getRelevance(i));
                i++;
            }
            else if(j<b.numDocuments()){
                result.appendDocWithRelevance((LinkedDocument)b.get(j), b.getQuerySimilarity(j), b.getRelevance(j));
                j++;
            }
        }
        return result;
    }

    /* add doc with relevance and similarity ( for sort by relevance) */
    private void appendDocWithRelevance(LinkedDocument doc, double sim, double rel){
        if(doc != null) { //if paramenter isn't null
            DocumentCollectionCell newDoc = new DocumentCollectionCell(doc, sim, rel); //make new cell
            if (getDocuments()== null) // if list is empty
                setDocuments(newDoc);
            else { // if not empty, set up counter temp
                //get to the lest document in the list and add newDoc to it
                DocumentCollectionCell temp = getDocuments();
                while (temp.getNext() != null)
                    temp = temp.getNext();
                temp.setNext(newDoc);
                newDoc.setPrev(temp);
            }
        }
    }

    /**
     * calculates similarity and sorts documents by relevance */
    public void match(String searchQuery){
        super.match(searchQuery);
        sortByRelevance(0.85, 0.6);
    }
}
