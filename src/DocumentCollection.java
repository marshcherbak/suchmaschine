public class DocumentCollection {

    public void setDocuments(DocumentCollectionCell documents) {
        this.documents = documents;
    }

    private DocumentCollectionCell documents;

    //getter
    public DocumentCollectionCell getDocuments() { return documents; }


    //constructor, initializes an empty collection (list)
    public DocumentCollection(){
        documents = null;
    }

    // this method adds a document at the start of the list if the given document isn't null
    public void prependDocument(Document doc){
        if(doc!=null){ //if paramenter isn't null
            DocumentCollectionCell cell = new DocumentCollectionCell(doc); //make new cell
            if(documents==null){ // if list empty
                documents = cell;
            }
            else { // if not empty, make cell the start of the list and previous start - a second element
                //cell.prev is already null (from cell constructor)
                cell.setNext(documents);
                documents.setPrev(cell);
                documents = cell;
            }
        }
    }

    // this method adds a document at the end of the list if the given document isn't null
    public void appendDocument(Document doc){
        if(doc != null) { //if paramenter isn't null
            DocumentCollectionCell newDoc = new DocumentCollectionCell(doc); //make new cell
            if (documents == null) // if list is empty
                documents = newDoc;
            else { // if not empty, set up counter temp
                //get to the lest document in the list and add newDoc to it
                DocumentCollectionCell temp = documents;
                while (temp.getNext() != null)
                    temp = temp.getNext();
                temp.setNext(newDoc);
                newDoc.setPrev(temp);
            }
        }
    }


    //this method returns true if the list is empty
    public boolean isEmpty(){
        return (documents==null);
    }

    //this method returns the amount of documents in the list/collection
    public int numDocuments(){
        DocumentCollectionCell temp = documents;
        int i = 0;
        while(temp!=null){
            temp=temp.getNext();
            i++;
        }
        return i;
    }

    /* this method returns the first document in the list
    *  if the list isn't empty
    *  returns null if the list is empty */
    public Document getFirstDocument(){
        if(documents==null)
            return null;
        else
            return documents.getInfo();
    }

    /*this method returns the last document in the list
    * if the list isn't empty
    * if it's empty, returns null */
    public Document getLastDocument(){
        if(documents==null)
            return null;
        else {
            DocumentCollectionCell temp = documents;
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            return temp.getInfo();
        }
    }

    /* this mothod removes the first document from the list
    *  does nothing if the list is empty */
    public void removeFirstDocument(){
        if(!isEmpty()) {
            if(numDocuments()==1) // if only one document remove it by setting documents to null
                documents = null;
            else { // else remove first doc and make the second one the first
                documents.getNext().setPrev(null);
                documents = documents.getNext();
            }
        }
    }

    /* this method removes the last document
    *  if the list is empty, does nothing */
    public void removeLastDocument(){
        if(!isEmpty()) {
            if(numDocuments()==1) // if only one document remove it by setting documents to null
                documents = null;
            else {
                DocumentCollectionCell temp = documents; //counter
                while (temp.getNext().getNext() != null) { // get to previous-to-last element
                    temp = temp.getNext();
                }
                temp.setNext(null); // make it the last, thus removing the last element in the list
            }
        }
    }

    /* removes the document at a set index (index start from 0) and returns true if it was successful
    *  if index is out of bound (greater than the amount of elements in the list), returns false
    *  if no documents in the list, returns false
    *  else removes the document at given index and returns true */
    public boolean remove(int index) {
        boolean flag = true;
        if (isEmpty() || index >= this.numDocuments() || index < 0)
            flag = false;
        else if (index == 0) {
            this.removeFirstDocument();
        }
        else if (index == this.numDocuments() - 1) {
            this.removeLastDocument();
        }
        else {
            DocumentCollectionCell temp = documents; //counter
            int j = 0;
            while (j < index - 1) { // get to previous element to the one at index
                temp = temp.getNext();
                j++;
            }
            //remove the document at imdex
            temp.setNext(temp.getNext().getNext());
            temp.getNext().setPrev(temp);
        }
        return flag;
    }

    /* returns the Document at given index (index starts at 0)
    *  if index is greater than the amount of elements or the list is empty, returns null */
    public Document get(int index){
        if(isEmpty() || index >= this.numDocuments() || index < 0)
            return null;
        else if (index == 0)
            return this.getFirstDocument();
        else if (index == numDocuments()-1)
            return this.getLastDocument();
        else{
            DocumentCollectionCell temp = documents;
            int j = 0;
            while(j < index){
                temp = temp.getNext();
                j++;
            }
            return temp.getInfo();
        }
    }

    /* this method returns the index of the document doc in the DocumentCollection (first document is at index 0)
    *  if doc isn't in the list, or the list is empty, returns -1
    *  if documents is in the collection more than once, will return the index of the first instance (the smallest index) */
    public int indexOf(Document doc){
        int i = 0;
        boolean found = false;
        DocumentCollectionCell temp = documents;
        while(temp!=null && !found){
            if(temp.getInfo().equals(doc)){
                found = true;
            }
            else{
                temp = temp.getNext();
                i++;
            }
        }
        if(found)
            return i;
        else
            return -1;
    }

    // this method returns true if doc is in the current documentcollection, false otherwise
    public boolean contains(Document doc){
        return this.indexOf(doc)!=-1;
    }

    /* this method returns a WordCountsArray containing of all words in the collection
    *  (each word is in at least one document)
    *  each word will be in the resulting WordCountsArray only once
    *  returns null if documentcollection is an empty list */
    private WordCountsArray allWords(){
        if(this.isEmpty())
            return null;
        else {
            WordCountsArray wca = new WordCountsArray(documents.getInfo().getWordCounts().size()); // initial size is the number of words in the first document
            DocumentCollectionCell temp = documents; //counter
            while (temp != null) {
                for (int i = 0; i < temp.getInfo().getWordCounts().size(); i++) {
                    //always adds words with count 0, since frequency is here irrelevant
                    wca.add(temp.getInfo().getWordCounts().getWord(i), 0);
                }
                temp = temp.getNext();
            }
            return wca;
        }
    }

    /* this method adds to each document in the collection all words that are in the collection
    *  each word is added with the count 0, so that if document already has this word, count won't change
    *  if the document didn't have the word, it'll have a count 0
    *  if collection is empty, does nothing */
    private void addZeroWordsToDocuments(){
        if(!isEmpty()){
            WordCountsArray wca = this.allWords();
            DocumentCollectionCell temp = documents; //counter
            while(temp!=null){ //goes through each document and adds all words in wca to the wordcountsarray of the document
                for(int i = 0; i < wca.size(); i++){
                    temp.getInfo().getWordCounts().add(wca.getWord(i), 0);
                }
                temp = temp.getNext();
            }
        }
    }

    /* this method calculates the similarity of each document to the searchQuery and then sorts the documents
    * from higher similarity to lower */
    public void match(String searchQuery){
        LinkedDocument d = new LinkedDocument("SearchQuery", "de", "", new Date(), new Author("", "", new Date(), "", "@"), searchQuery, "id");
        this.prependDocument(d); // make a doc from searchQuery and add it to the start of the list

        this.addZeroWordsToDocuments(); //add all words to all docs
        DocumentCollectionCell temp = documents;
        while(temp!=null){ // sort words in all documents
            temp.getInfo().getWordCounts().sort();
            temp = temp.getNext();
        }

        temp = documents.getNext();
        while(temp!=null){
            //calculate similarity to searchQuery
            double similarity = documents.getInfo().getWordCounts().computeSimilarity(temp.getInfo().getWordCounts(), this);
            temp.setSimilarity(similarity);
            temp = temp.getNext();
        }
        removeFirstDocument(); //remove doc containing search query
        sortBySimilarityDesc(); // sort documents
    }

    /* this method returns the last calculated similarity of the document at given index
    *  here the similarity will be the one calculated for the searchQuery
    *  that was in the last called method match(String searchQuery)
    *  if the collection is empty or index is illegal, returns -1 */
    public double getQuerySimilarity(int index){
        if(isEmpty() || index >= numDocuments() || index < 0)
            return -1;
        else {
            DocumentCollectionCell temp = documents;
            int i = 0;
            while(i < index){
                temp = temp.getNext();
                i++;
            }
            return temp.getSimilarity();
        }
    }


    //sorts documents in the collection using iterative merge sort
    private void sortBySimilarityDesc() {
        DocumentCollection [] tempc = new DocumentCollection[this.numDocuments()]; //array of collections

        for(int i = 0; i < this.numDocuments(); i++){ //array of collection, each consisting of a single document
            tempc[i] = new DocumentCollection();
            tempc[i].appendDocWithSim(this.get(i), this.getQuerySimilarity(i));
        }

        while(tempc.length!=1){
            int n = tempc.length/2 + (tempc.length%2);
            DocumentCollection [] nextMerge = new DocumentCollection[n];
            int j = 0;
            int i = 0;
            while(i < tempc.length){
                if((i+1)==tempc.length)
                    nextMerge[j]= tempc[i];
                else {
                    nextMerge[j] = mergeLists(tempc[i], tempc[i + 1]);
                }
                i+=2;
                j++;
            }
            tempc = nextMerge;
        }
        this.documents = tempc[0].getDocuments();
    }

    /* additional method for sort: adds to the end of the list a documentCollectionCell that has a document and a set similarity
    * does nothing if the document == null */
    private void appendDocWithSim(Document doc, double sim){
        if(doc != null) { //if paramenter isn't null
            DocumentCollectionCell newDoc = new DocumentCollectionCell(doc, sim); //make new cell
            if (documents == null) // if list is empty
                documents = newDoc;
            else { // if not empty, set up counter temp
                //get to the lest document in the list and add newDoc to it
                DocumentCollectionCell temp = documents;
                while (temp.getNext() != null)
                    temp = temp.getNext();
                temp.setNext(newDoc);
                newDoc.setPrev(temp);
            }
        }
    }

    /* this method merges two sorted collections into one (sorted)*/
    private DocumentCollection mergeLists(DocumentCollection a, DocumentCollection b){
        DocumentCollection result = new DocumentCollection();
        int num = a.numDocuments()+b.numDocuments();
        int i = 0;
        int j = 0;
        while(result.numDocuments()!=num){
            if(i!=a.numDocuments() && j!=b.numDocuments()){
                if(a.getQuerySimilarity(i)>=b.getQuerySimilarity(j)){ //add bigger element to resulting collection
                    result.appendDocWithSim(a.get(i), a.getQuerySimilarity(i));
                    i++;
                }
                else{
                    result.appendDocWithSim(b.get(j), b.getQuerySimilarity(j));
                    j++;
                }
            }
            else if (i<a.numDocuments()){
                result.appendDocWithSim(a.get(i), a.getQuerySimilarity(i));
                i++;
            }
            else if(j<b.numDocuments()){
                result.appendDocWithSim(b.get(j), b.getQuerySimilarity(j));
                j++;
            }
        }
        return result;
    }

    /* this method returns the amount of documents in the collection
    *  that have the word word in them with the frequency >= 1*/
    public int noOfDocumentsContainingWord(String word){
        int res = 0;
        if(word!=null) {
            DocumentCollectionCell temp = documents;
            while (temp != null) {
                int i = temp.getInfo().getWordCounts().getIndexOfWord(word);
                if (i != -1) {
                    int f = temp.getInfo().getWordCounts().getCount(i);
                    if (f >= 1)
                        res++;
                }
                temp = temp.getNext();
            }
        }
        return res;
    }



}
