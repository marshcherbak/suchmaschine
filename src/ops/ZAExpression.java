package ops;

abstract public class ZAExpression<T> {
    private T value;
    public boolean evaluated = false;
    public T getValue(){
        if(!evaluated)
            evaluate();
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    abstract public T evaluate();
    abstract public String toString();
}
