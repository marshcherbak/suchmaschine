package ops;

public class SubOp<T> extends BinOp {
    public SubOp(Expression a, Expression b) {
        super(a, b);
    }

    /* assume T is always Integer */
    public Integer evaluate(){
        Integer ia = (Integer)a.getValue();
        Integer ba = (Integer)b.getValue();
        Integer res = ia - ba;
        evaluated = true;
        setValue(res);
        return (Integer)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " - " + b.toString() + ")";
    }
}
