package ops;

public class ZAConst<T> extends ZAExpression {
    public ZAConst(T t){
        setValue(t);
    }

    public T evaluate(){
        evaluated = true;
        return (T)getValue();
    }

    public String toString(){
        return getValue().toString();
    }
}
