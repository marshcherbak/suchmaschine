package ops;

import ops.BinOp;

public class DivOp<T> extends BinOp {

    public DivOp(Expression a, Expression b) {
        super(a, b);
    }

    /* assume we are always working with Integers */
    public Integer evaluate(){
        Integer ia = (Integer)a.getValue();
        Integer ib = (Integer)b.getValue();
        Integer res = ia/ib;
        setValue(res);
        evaluated = true;
        return (Integer)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " / " + b.toString() + ")";
    }
}
