package ops;

abstract public class ZABinOp<T1, T2> extends ZAExpression<T2> {
    protected ZAExpression<T1> a;
    protected ZAExpression<T2> b;

    /*Expressions can have different type of parameters */
    public ZABinOp(ZAExpression a, ZAExpression b){
        this.a = a;
        this.b = b;
    }
}
