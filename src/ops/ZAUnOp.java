package ops;

abstract public class ZAUnOp<T> extends ZAExpression<T> {
    ZAExpression<T> t;

    public ZAUnOp(ZAExpression t){
        this.t = t;
    }

}
