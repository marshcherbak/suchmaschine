package ops;

public class OrOp<T> extends BinOp {
    public OrOp(Expression a, Expression b) {
        super(a,b);
    }

    /* assume T is always boolean*/
    public Boolean evaluate(){
        Boolean ba = (Boolean)a.getValue();
        Boolean bb = (Boolean)b.getValue();
        Boolean res = ba || bb;
        setValue(res);
        evaluated = true;
        return (Boolean)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " || " + b.toString() + ")";
    }
}
