package ops;

public class ZATest {
    public static void main(String[] args) {
        ZAExpression<Boolean> exp = new ZAEQOp<Integer, Boolean>(new
                ZAMulOp<Integer, Integer>(new ZAConst<Integer>(3),
                 new ZAAddOp<Integer, Integer>(new ZAConst<Integer>(1), new
                 ZAConst<Integer>(2))), new ZAConst<Integer>(9));
        System.out.print(exp);
        System.out.println(" = " + exp.evaluate());
        System.out.println();

        ZAExpression<Boolean> exp2 = new ZAGTOp<Integer, Boolean>(new ZAMulOp<Integer, Integer>(new ZAConst<Integer>(4),
                new ZAAddOp<Integer, Integer>(new ZAConst<Integer>(1), new ZAConst<Integer>(3))),
                new ZAAddOp<Integer, Integer>(new ZAConst<Integer>(4), new ZADivOp<Integer,Integer>(new ZAConst<Integer>(8),
                        new ZAConst<Integer>(2))));
        System.out.print(exp2);
        System.out.println(" = " + exp2.evaluate());
        System.out.println();

        ZAExpression<Boolean> exp3 = new ZALTOp<Integer, Boolean>(new ZAAddOp<Integer,Integer>(new ZAConst<Integer>(1), new ZASubOp<Integer,Integer>(new ZAConst<Integer>(5),
                        new ZAConst<Integer>(3))),new ZADivOp<Integer,Integer>(new ZAAddOp<Integer,Integer>(
                new ZAConst<Integer>(10), new ZAConst<Integer>(8)), new ZANegOp<Integer>(new ZAConst<Integer>(2))));
        System.out.print(exp3);
        System.out.println(" = " + exp3.evaluate());
        System.out.println();

        ZAExpression<Boolean> exp4 = new ZAOrOp<Boolean,Boolean>(new ZAEQOp<Boolean,Boolean>(new ZAConst<Boolean>(true),
                new ZAConst<Boolean>(false)), new ZAAndOp<Boolean,Boolean>(new ZAOrOp<Boolean,Boolean>(new ZAConst<Boolean>(false),
                new ZAConst<Boolean>(true)), new ZAConst<Boolean>(true)));
        System.out.print(exp4);
        System.out.println(" = " + exp4.evaluate());
        System.out.println();

        ZAExpression<Boolean> exp5 = new ZAEQOp<Boolean, Boolean>(new ZAOrOp<Boolean,Boolean>(new ZAConst<Boolean>(false),
                new ZAConst<Boolean>(false)), new ZAAndOp<Boolean,Boolean>(new ZAOrOp<Boolean,Boolean>(new ZAConst<Boolean>(false),
                new ZAConst<Boolean>(true)), new ZAConst<Boolean>(true)));
        System.out.print(exp5);
        System.out.println(" = " + exp5.evaluate());
        System.out.println();
    }
}
