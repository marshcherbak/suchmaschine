package ops;

abstract public class UnOp<T> extends Expression{

    Expression<T> t;

    public UnOp(Expression t){
        this.t = t;
    }

}
