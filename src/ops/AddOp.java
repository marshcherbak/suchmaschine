package ops;

public class AddOp<T> extends BinOp {
    public AddOp(Expression a, Expression b){
        super(a,b);
    }

    @Override
    /* assume in this class we are always working with Integers */
    public Integer evaluate(){
        Integer m = (Integer)a.getValue()+(Integer)b.getValue();
        evaluated = true;
        setValue(m);
        return (Integer)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " + " + b.toString() + ")";
    }
}
