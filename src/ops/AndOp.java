package ops;

public class AndOp<T> extends BinOp {
    public AndOp(Expression a, Expression b) {
        super(a,b);
    }

    /* assume we are always working with Boolean*/
    public Boolean evaluate(){
        Boolean k = (Boolean)a.getValue() && (Boolean)b.getValue();
        setValue(k);
        evaluated = true;
        return (Boolean)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " && " + b.toString() + ")";
    }
}
