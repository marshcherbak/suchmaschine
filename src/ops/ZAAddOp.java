package ops;

public class ZAAddOp<T1, T2> extends ZABinOp {
    public ZAAddOp(ZAExpression a, ZAExpression b){
        super(a,b);
    }

    @Override
    /*T1 and T2 alwways have to be Integer */
    public Integer evaluate(){
        Integer m = (Integer)a.getValue()+(Integer)b.getValue();
        evaluated = true;
        setValue(m);
        return (Integer)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " + " + b.toString() + ")";
    }
}
