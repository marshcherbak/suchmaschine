package ops;

public class ZANegOp<T> extends ZAUnOp {
    public ZANegOp(ZAExpression t) {
        super(t);
    }

    @Override
    public T evaluate() {
        if(t.getValue() instanceof Integer) {
            Integer res = (-(Integer) t.getValue());
            setValue(res);
        }
        else {
            Boolean res = (!(Boolean) t.getValue());
            setValue(res);
        }
        evaluated = true;
        return (T)getValue();
    }


    public String toString(){
        return "(-" + t.toString() + ")";
    }
}
