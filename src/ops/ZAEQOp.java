package ops;

public class ZAEQOp<T1, T2> extends ZABinOp {
    public ZAEQOp(ZAExpression a, ZAExpression b){
        super(a,b);
    }

    /*T1 is the type of expressions and T2 is the result (Boolean)*/
    public Boolean evaluate(){
        Boolean res = false;
        if((T1)a.getValue() == (T1)b.getValue()) {
            res = true;
        }
        setValue(res);
        evaluated = true;
        return res;
    }

    public String toString(){
        return "(" + a.toString() + " == " + b.toString() + ")";
    }
}
