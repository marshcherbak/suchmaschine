package ops;

public class MulOp<T> extends BinOp {
    public MulOp(Expression a, Expression b){
        super(a,b);
    }

    /*assume T is always of type Integer*/
    public Integer evaluate(){
        Integer res = (Integer)a.getValue()* (Integer)b.getValue();
        evaluated = true;
        setValue(res);
        return (Integer)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " * " + b.toString() + ")";
    }
}
