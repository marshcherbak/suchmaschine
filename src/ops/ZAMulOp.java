package ops;

public class ZAMulOp<T1, T2> extends ZABinOp {
    public ZAMulOp(ZAExpression a, ZAExpression b){
        super(a,b);
    }

    /* T1 is the type of expressions, has to be the same as T2 */
    public Integer evaluate(){
        Integer res = (Integer)a.getValue()* (Integer)b.getValue();
        evaluated = true;
        setValue(res);
        return (Integer)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " * " + b.toString() + ")";
    }
}
