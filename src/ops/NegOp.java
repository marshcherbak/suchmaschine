package ops;

public class NegOp<T> extends UnOp {
    public NegOp(Expression t) {
        super(t);
    }

    @Override

    public T evaluate() {
        /* if T is of type integer return a negative number */
        if(t.getValue() instanceof Integer) {
            Integer res = (-(Integer) t.getValue());
            setValue(res);
            evaluated = true;
        }
        /* if T is of type Boolean returns the opposite*/
        else {
            Boolean res = (!(Boolean) t.getValue());
            setValue(res);
            evaluated = true;
        }
        return (T)getValue();
    }


    public String toString(){
        return "(-" + t.toString() + ")";
    }
}
