package ops;

abstract public class Expression <T> {
    private T value;
    public boolean evaluated = false;

    /* get's value if the expression was already evaluated. if not, evaluates it and returns the value */
    public T getValue(){
        if(!evaluated)
            evaluate();
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    abstract public T evaluate();
    abstract public String toString();
}
