package ops;

abstract public class BinOp<T> extends Expression<T> {
    protected Expression<T> a; //left expression
    protected Expression<T> b; //right expression
    public BinOp(Expression a, Expression b){
        this.a = a;
        this.b = b;
    }
}
