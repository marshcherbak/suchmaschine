package ops;

public class ZASubOp<T1, T2> extends ZABinOp {
    public ZASubOp(ZAExpression a, ZAExpression b) {
        super(a, b);
    }

    public Integer evaluate(){
        Integer ia = (Integer)a.getValue();
        Integer ba = (Integer)b.getValue();
        Integer res = ia - ba;
        evaluated = true;
        setValue(res);
        return (Integer)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " - " + b.toString() + ")";
    }
}
