package ops;

public class ZADivOp<T1, T2> extends ZABinOp {
    public ZADivOp(ZAExpression a, ZAExpression b) {
        super(a, b);
    }

    /*T1 and T2 are Integers*/
    public Integer evaluate(){
        Integer ia = (Integer)a.getValue();
        Integer ib = (Integer)b.getValue();
        Integer res = ia/ib;
        setValue(res);
        evaluated = true;
        return (Integer)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " / " + b.toString() + ")";
    }
}
