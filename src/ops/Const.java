package ops;

public class Const <T> extends Expression{
    public Const(T t){
        setValue(t);
    }

    public T evaluate(){
        evaluated = true;
        return (T)getValue();
    }

    public String toString(){
        return getValue().toString();
    }
}
