package ops;

public class ZAOrOp<T1,T2> extends ZABinOp {
    public ZAOrOp(ZAExpression a, ZAExpression b) {
        super(a,b);
    }

    public Boolean evaluate(){
        Boolean ba = (Boolean)a.getValue();
        Boolean bb = (Boolean)b.getValue();
        Boolean res = ba || bb;
        setValue(res);
        evaluated = true;
        return (Boolean)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " || " + b.toString() + ")";
    }
}
