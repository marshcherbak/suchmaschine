package ops;

public class ATest {
    public static void main(String[] args) {
        System.out.println("Blatt08 examples:" + Terminal.NEWLINE);
        Expression<Integer> a = (new MulOp<Integer>(new Const<Integer>(3), new AddOp<Integer>(new
                Const<Integer>(1), new Const<Integer>(2))));
        System.out.print(a);
        System.out.println(" = " + a.evaluate());
        System.out.println();

        Expression<Boolean> b = (new AndOp<Boolean>(new Const<Boolean>(true),new OrOp<Boolean>(new
                Const<Boolean>(false), new Const<Boolean>(true))));
        System.out.print(b);
        System.out.println(" = " + b.evaluate());
        System.out.println();

        System.out.println("Own examples:" + Terminal.NEWLINE);
        Expression<Integer> c = new DivOp<Integer>(new MulOp<Integer>(new Const<Integer>(4), new Const<Integer>(4)),
                new AddOp<Integer>(new Const<Integer>(2), new MulOp<Integer>(new Const<Integer>(3), new Const<Integer>(2))));
        System.out.print(c);
        System.out.println(" = " + c.evaluate());
        System.out.println();

        Expression<Integer> d = new MulOp<Integer>(new AddOp<Integer>(new Const<Integer>(1), new SubOp<Integer>(new Const<Integer>(5),
                new Const<Integer>(3))), new DivOp<Integer>(new AddOp<Integer>(new Const<Integer>(10), new Const<Integer>(8)),
                new NegOp<Integer>(new Const<Integer>(2))));
        System.out.print(d);
        System.out.println(" = " + d.evaluate());
        System.out.println();

        Expression<Boolean> e = new AndOp<Boolean>(new Const<Boolean>(true), new OrOp<Boolean>(new NegOp<Boolean>(new Const<Boolean>(false)),
                new AndOp<Boolean>(new OrOp<Boolean>(new Const<Boolean>(true), new Const<Boolean>(false)),
                        new Const<Boolean>(true))));
        System.out.print(e);
        System.out.println(" = " + e.evaluate());
        System.out.println();

        Expression<Boolean> f = new OrOp<Boolean>(new AndOp<Boolean>(new Const<Boolean>(true), new Const<Boolean>(false)),
                new OrOp<Boolean>(new Const<Boolean>(false), new AndOp<Boolean>(new NegOp<Boolean>(new Const<Boolean>(true)),
                        new Const<Boolean>(false))));
        System.out.print(f);
        System.out.println(" = " + f.evaluate());
        System.out.println();
    }
}
