package ops;

public class ZAAndOp<T1,T2> extends ZABinOp{
    public ZAAndOp(ZAExpression a, ZAExpression b) {
        super(a,b);
    }

    /* T1 and T2 always have to be Boolean */
    public Boolean evaluate(){
        Boolean k = (Boolean)a.getValue() && (Boolean)b.getValue();
        setValue(k);
        evaluated = true;
        return (Boolean)getValue();
    }

    public String toString(){
        return "(" + a.toString() + " && " + b.toString() + ")";
    }
}
