package ops;

public class ZALTOp<T1, T2> extends ZABinOp {
    public ZALTOp(ZAExpression<T1> a, ZAExpression<T2> b){
        super(a,b);
    }

    /* T1 is the type of expression the operation evaluates and T2 is the result (Boolean) */
    public Boolean evaluate(){
        Boolean res = false;
        if((Integer)a.getValue() < (Integer)b.getValue()) {
            res = true;
        }
        setValue(res);
        evaluated = true;
        return res;
    }

    public String toString(){
        return "(" + a.toString() + " < " + b.toString() + ")";
    }
}
