public class Test {
    public static void main(String[] args) {
        Test t = new Test();
       // WordCountsArray words = new WordCountsArray(3);
        //t.testWordCounts(words);
        //the only open new method in Document is the constructor itself
        Document d = new Document("hello", "en", "jjjj", new Date(),
                new Author("l", "m", new Date(), "", ""), "Sie für diese Aufgabe davon aus dass der Text text ausschließlich Kleinbuchstaben " +
                "und Leerzeichen enthält Zwei Wörter sind dabei jeweils durch genau ein Leerzeichen " +
                "voneinander getrennt Die Anzahl der Wörter im übergebenen Text ergibt sich aus " +
                "der Anzahl der Leerzeichen plus 1");
        d.getWordCounts().sort();
        t.printWords(d.getWordCounts());
        /*System.out.println("Test words for document: ");
        words = d.getWordCounts();
        t.printWords(words);
        t.testWordCounts(words);

        String s = "In fact, if you put oy and poodle together in the same sentence, you’d have a great new catch phrase, you know? Like, ‘Oy, with the poodles already.’ So from now on, when the perfect circumstances arise, we will use our favorite new catch phrase.";
        d = new Document("Episode 1.22", "en", "Gilmore girls season 1", new Date(1,1,2002),
                new Author("Amy", "Sherman-Palladino", new Date(), "USA", "amy@com"), s);

        t.printWords(d.getWordCounts());
        d = new Document("Episode 1.22", "en", "Gilmore girls season 1", new Date(1,1,2002),
                new Author("Amy", "Sherman-Palladino", new Date(-5,3,1222), "USA", "amy@com"), "");

        t.printWords(d.getWordCounts());*/
    }

    public void printWords(WordCountsArray words){
        System.out.println(Terminal.NEWLINE + "Words:");
        for(int i = 0; i < words.size(); i++){
            System.out.println(words.getWordCountArray()[i].getWord() + " " + words.getWordCountArray()[i].getFrequency() + ", ");
        }
    }
    public void testAdd(WordCountsArray words, String w, int c){
        System.out.println(Terminal.NEWLINE + "Testing add:");
        System.out.println("Length = " + words.getWordCountArray().length + "; Size: " + words.size());
        printWords(words);
        System.out.println("Adding word: " + "\"" + w + "\"" + " with frequency " + c);
        words.add(w,c);
        System.out.println("If everything is correct, size or size and length will change. If frequency < 0, will add word with frequency 0.");
        System.out.println("New: Length = " + words.getWordCountArray().length + "; Size: " + words.size());
        printWords(words);
    }
    public void testGetCount(WordCountsArray words, int index){
        System.out.println(Terminal.NEWLINE + "Testing GetCount:");
        System.out.println("Frequency of the word at position " + index + " is " + words.getCount(index));
    }
    public void testGetWord(WordCountsArray words, int i){
        System.out.println(Terminal.NEWLINE + "Testing GetWord:");
        System.out.println("Word at position " + i + " is " + "\"" + words.getWord(i) + "\"");
    }
    public void testSetCount(WordCountsArray words, int i, int count){
        System.out.println(Terminal.NEWLINE + "Testing SetCount:");
        printWords(words);
        System.out.println("If i = " +i + " exitsts and count = " + count + " is valid, will set new frequency for word at given index.");
        System.out.println("Word at position " + i + " is " + words.getWord(i) + " " + words.getCount(i));
        words.setCount(i,count);
        System.out.println("Word at position " + i + " is " + words.getWord(i) + " " + words.getCount(i));
    }
    private void testWordCounts (WordCountsArray words){
        testAdd(words,"", 5);
        testAdd(words,"hello", 1);
        testAdd(words,"mama", -5);
        testAdd(words,"oy", 0);

        testGetCount(words,words.getWordCountArray().length-1);
        testGetCount(words,words.size()+10);
        testSetCount(words,5,10);
        testSetCount(words,words.size()-1, 10);
        testGetWord(words,2);
        testGetWord(words, 30);
    }



}
