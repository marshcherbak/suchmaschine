/**
 * The class {@code Date} represents a date.
 * 
 * @author Florian Kelbert
 *
 */
public class Date {
    /* defaults for when a part of the date is invalid are -1*/
	private int day;
	public int getDay() {
		return this.day;
	}
	public void setDay(int day) { // checks if given day with already set month and year will make a valid date
		//if yes, sets new day to the date
		//if no, doesn't do anything
		if(isValidDate(day, this.month, this.year))
		    this.day = day;
	}

	private int month;
	public int getMonth() {
		return this.month;
	}
	public void setMonth(int month) { // checks if given month with already set day and year will make a valid date
		//if yes, sets new month to the date
		//if no, doesn't do anything
	    if(isValidDate(this.day, month, this.year))
		    this.month = month;
	}

	private int year;
	public int getYear() {
		return this.year;
	}
	public void setYear(int year) { // checks if given year with already set month and day will make a valid date
		//if yes, sets new year to the date
		//if no, doesn't do anything
	    if(isValidDate(this.day, this.month, year))
		    this.year = year;
	}

	public Date(int day, int month, int year) { //checks if the date is valid (including checking if this is a leap year and February 29th exists using isLeapYear method.
	    // if Feb 29th is given in a non-leap year or any other date parts are invalid sets the date to default: -1.-1.-1
        if(isValidDate(day, month, year)){
            this.day = day;
            this.year = year;
            this.month = month;
        }
        else {
            this.day = -1;
            this.month = -1;
            this.year = -1;
        }
	}
	
	public Date() {
		this.day = Terminal.TODAYS_DAY;
		this.month = Terminal.TODAYS_MONTH;
		this.year = Terminal.TODAYS_YEAR;
	}
	
	public String toString() {
		return "Date: " + this.day + "." + this.month + "." + this.year;
	}
	
	private int daysSince1970() { // days between current instance of the class and 1.1.1970, no leap years, months have 30 days
		int years = this.year-1970;
		//int months = this.month - 1;
		int days = this.day - 1;
		int result = 0;
		int febDays;
		if(isLeapYear(this.year))
		    febDays = 29;
		else
		    febDays = 28;

		for (int i = 1970; i < this.year; i++){
		    if(isLeapYear(i))
		        result+=366;
		    else
		        result+=365;
        }
		switch(this.month){
			case 2:
				result+=(31);
				break;
			case 3:
				result += (31 + febDays);
				break;
			case 4:
				result += (2*31 + febDays);
				break;
            case 5:
                result += (2*31 + febDays + 30);
                break;
            case 6:
                result += (3*31 + febDays + 30);
                break;
            case 7:
                result += (3*31 + febDays + 2*30);
                break;
            case 8:
                result += (4*31 + febDays + 2*30);
                break;
            case 9:
                result += (5*31 + febDays + 2*30);
                break;
            case 10:
                result += (5*31 + febDays + 3*30);
                break;
            case 11:
                result += (6*31 + febDays + 3*30);
                break;
            case 12:
                result += (6*31 + febDays + 4*30);
                break;
            default:
                break;
        }

		return result + days;
	}
	
	public int getAgeInDaysAt(Date today) { //days between today and the instance of the date from which the method is called
		//gets days by counting days since 1970 to today, since 1970 to the date of the instance, und counting the difference
		return today.daysSince1970() - this.daysSince1970();
	}
	
	public int getAgeInYearsAt(Date today) { // same as previous but *full* years
	    int years = 0;
	    int i = this.getAgeInDaysAt(today);
		int birthyear = today.year;
		int y = Terminal.TODAYS_YEAR;
		int k;
		if(isLeapYear(y))
		    k = 366;
		else
		    k = 365;
		while(i > k){
		    if(isLeapYear(birthyear))
		        i-=366;
		    else
		        i-=365;
		    birthyear++;
		    years++;
        }
        if(this.month==Terminal.TODAYS_MONTH && this.day == Terminal.TODAYS_DAY) //if the birthday/date is today, + one year
            years++;
	    return years;
	}

	private boolean isValidDate(int day, int month, int year){ // checks if the given date exists
        boolean flag = false;
        if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12){
            if(day >=1 && day <= 31)
                flag = true;
        }
        else if(month == 2){
            if(isLeapYear(year)){
                if(day >= 1 && day <=29)
                    flag = true;
            }
            else {
                if (day >= 1 && day <= 28)
                    flag = true;
            }
        }
        else if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day >= 1 && day <= 30)
                flag = true;
        }
    return flag;

    }
	private boolean isLeapYear(int year){ // checks is the given year is a leap year
		if (year%4 == 0) {
			if (year % 100 == 0 && year % 400 != 0)
				return false;
			return true;
		}
		return false;
	}

	public boolean equals(Date date){
		if(date!=null){
			if (this.year == date.getYear() && this.month == date.getMonth() && this.day == date.getDay())
				return true;
		}
		return false;
	}
	public static void main(String[] args) {
        Date d = new Date(31,12,1973); //sets this date
        Date c = new Date();

        System.out.println(d.getAgeInDaysAt(c));
        System.out.println(d.getAgeInYearsAt(c));
        d.setDay(30); // won't change anything
        d.setMonth(4); //changes month to 4
        Date d2 = new Date(30,2,2018); //sets to default values: -1.-1.-1
    }
}


/* It's necessary to write good documentation and explain what's happening in different methods,
* because most programs don't get written in a day (or by one person) and when you go back to your code, or try to
* change something, or understand why some mistake is happening, it's extremely important to have information
* about what's going on in different methods and how they function. It makes it easier to understand the code,
* especially when some time passed since writing it, or if another person is reading the code.
* For example, in methods that count age in days and years (for documents/reviews and authors respectively)
* it's not entirely clear how they function and some of the conditions (like all months having 30 days) that they have,
* so it makes sense to add a comment that explains them, so that when going back, it's enough to
* just read the comment without trying to trace back to where the method inside the method comes from.*/