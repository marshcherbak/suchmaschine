import java.sql.SQLOutput;

public class Stringulina {
    /* this method finds the position of the first finding of the needle in haystack.
    * if needle is not in the haystack, returns -1 */
    public static int substringPos(String haystack, String needle){
        if(needle.length() > haystack.length()) //if needle is longer than haystack, returns -1
            return -1;
        int j = 0;
        int ind = -1;
        for(int i = 0; i < haystack.length() && j < needle.length(); i++){
            /* comparison character by character. If there's the same character, compare all next. If we get to the length
            * of the string needle - word found */
            if(haystack.charAt(i)==needle.charAt(j)){
                if(ind == -1)
                    ind = i;
                j++;
            }
            /* if there's a different character, go back in the string haystack
            to where we found the first char of string needle and start from the next one*/
            else {
                i = i-j;
                ind = -1;
                j=0;
            }
        }
        if(j==needle.length()) // if we got to the end of string needle, return the position of the first finding
            return ind;
        else
            return -1;
    }

    /* this method finds how often can we see the string needle in the string haystack*/
    public static int countSubstring(String haystack, String needle){
        int sum = 0;
        String s = stringFromPos(haystack, 0); //copy string so that we don't change the original
        int i = 0;
        while(i!=-1) {
            i = substringPos(s, needle); //if we find string needle in haystack
            if (i != -1) {
                sum++;
                s = stringFromPos(s, i+1); // cut off part of the word up so that now it starts with the char
                //at the position next to the first finding of the string needle
            }
        }
        return sum;
    }

    /*this method takes the string and a position and returns a string that is part of the original one,
    * starting from the position pos
    * if pos is illegal (eg. larger than the length of the string), returns an empty string */
    private static String stringFromPos(String str, int pos){

        String s = "";
        if(pos < str.length() && pos >= 0) {
            for (int i = pos; i < str.length(); i++) {
                s += str.charAt(i);
            }
        }
        return s;
    }

    public static String subString(String str, int start, int end){
        String s =  "";
        if(start>= 0 && end <=str.length()){
            for(int i = start; i <=end; i++)
                s+=str.charAt(i);
        }
        return s;
    }
    /*this method takes a word and checks if it's correctly bracketed */
    public static boolean correctlyBracketed(String str){
        int i = 0;
        int count = 0;
        while (count >= 0 && i < str.length()){ //stops if there's a closing bracket before the opening one or if reached end of the string
            if(str.charAt(i) == '(')
                count++;
            else if(str.charAt(i) == ')')
                count--;
            i++;
        }
        /*if we got to 0, it means the amount of opening brackets and closing brackets is even, and no closing bracket came before the opening one*/
        return count == 0;
    }

    /* this mathod checks if the string str maches the pattern with the pre-set rules for patterns:
    * x{n} = character x n times
    * . matches any character */
    public static boolean matches(String str, String pattern){
        int n = countSubstring(pattern, "{"); //get how times there's a repeating letter rule
        String s = stringFromPos(pattern, 0); // copy string
        for(int i = 0; i < n; i++){ //one by one transform each x{n} part into the actual line of letters
            // in the end the pattern will just be a string with letters and dots
            s = transformToPattern(s);
        }
        int i = 0;
        if(str.length() == s.length()){ /* compare string str and what we got from the patters
            * since now the {n}s are transformed into the needed amount of letters, we only compare characters
            * with the rule that the dot can be any character */
            boolean flag = true;
            while(flag && i < str.length()){
                if((str.charAt(i)== s.charAt(i) || s.charAt(i)=='.'))
                    i++;
                else
                    flag = false;
            }
        }
        //if we got the the end of the string then they are matching
        return i==str.length();
    }

    /* this method returns a string where the first found instance of the x{,} is transformed into the pattern it sets,
    * eg. a string ab{3}k{2} into abbbk{2}.*/
    private static String transformToPattern(String str){
        /*first part of the method gets the number inside {} and transforms it into integer*/
        String num = "";
        int f = substringPos(str, "{");
        if(f==-1)
            return str;
        int l = substringPos(str, "}");
        int i = f+1;
        while(i!=l){
            num+=str.charAt(i);
            i++;
        }
        int numOfDig = Integer.parseInt(num);
        String patt = "";
        int j = 0;
        //add all letters before the one we get the amount of from {}
        for(j = 0; j < f-1; j++) {
            patt += str.charAt(j);
        }
        //next add the needed amount of the character before {}
        char c = str.charAt(f-1);
        int t = j;
        for(;j < numOfDig+t; j++){
            patt+=c;
        }
        //add the remaining part of the string
        for(int k = l+1; k < str.length(); k++){
            patt+=str.charAt(k);
        }
        return patt;
    }
}
