public class Primverquerung {

    public static int querPrim(int n){ // gives the sum of all prime numbers < n, whose sum of digits is even
        // if n <= 2, returns 0, since there are no prime numbers that meet the condition before n = 3
        int sum = 0;
        if(n >= 3) {
            for(int i = 0; i < n; i++){
                if(isPrimeNumber(i)){ // checks if i is a prime number
                    int s = digitSum(i); // if yes, counts the sum of its digits
                    if (s%2 == 0) {
                        sum += i; //if it's even, add the number to the final result
                    }
                }
            }
        }
        return sum;
    }

    private static boolean isPrimeNumber(int n){ // checks if a given number is prime, using some common mathematical rules.
        int m = n%10; //get last digit
        int sum = digitSum(n); // to use for the rule that if a number is divisible by 3, its digit sum is also divisible by 3
        if(n == 2 || n == 3)
            return true;
        else if(m%2 == 0 || m==0 || m==5 || sum%3 == 0 || n == 1) // some common rules - if a number can be divided by 2, 10, 5 or 3, it's not prime
            return false;
        else { //check if the number can be divided by any other number. if not, it's prime
            int i = 4;
            boolean flag = true;
            while (flag && i < n){
                if(n%i == 0)
                    flag = false;
                i++;
            }
            return flag;
        }
    }
    private static int digitSum(int n){ //get the sum of the digits of the number
        int s = 0;
        if(n<10)
            return n;
        else {
            while (n != 0) {
                s += n % 10; // add last digit to the sum
                n = n / 10; // division by 10 gets rid of the last digit
            }
        }
        return s;
    }

    public static void main(String[] args) {
        int a = 312;
        int b = 0;
        int c = 997;
        int d = 10991;
        int e = 1234567891;
        System.out.println("querPrim for " + a + " gives " + querPrim(a)); // check for 312
        System.out.println("querPrim for " + b + " gives " + querPrim(b));
        System.out.println("querPrim for " + c + " gives " + querPrim(c));
        System.out.println("querPrim for " + d + " gives " + querPrim(d));
        System.out.println("Is " + b + " a prime number? " + isPrimeNumber(b)); // 0 is not prime
        System.out.println("Is " + c + " a prime number? " + isPrimeNumber(c)); // is prime
        System.out.println("Is " + d + " a prime number? " + isPrimeNumber(d));
        System.out.println("Sum of digits of " + d + " = " + digitSum(d));
        System.out.println("Sum of digits of " + c + " = " + digitSum(c));
        System.out.println("Sum of digits of " + e + " = " + digitSum(e));
    }
}
