import java.util.*;
import java.util.stream.*;

public class TemplateProcessor {
    private String text;

    public TemplateProcessor(String filename){
        String[] str = Terminal.readFile(filename);
        ArrayList<String> lines = new ArrayList<>(Arrays.asList(str));
        text = lines.stream().collect(Collectors.joining("\r\n"));
    }

    public String replace(java.util.Map<String, String> variableAssignments){
        variableAssignments.forEach((key,value)->text = text.replaceAll(key, value));
        return text;
    }

    public static void main(String[] args) {
        TemplateProcessor tp = new TemplateProcessor("search.html");
        java.util.TreeMap<String, String> varass = new java.util.TreeMap<>();
        varass.put("%searchvalue", "einfach");
        varass.put("%table", "<tr><td>1</td><td><a href=\"D.txt\">D.txt</a></td>\n" +
                "<td>0.3065295545993977</td></tr>\n" +
                "<tr><td>2</td><td><a href=\"C.txt\">C.txt</a></td>\n" +
                "<td>0.1758871913668732</td></tr>\n" +
                "<tr><td>3</td><td><a href=\"E.txt\">E.txt</a></td>\n" +
                "<td>0.16589500683583558</td></tr>\n" +
                "<tr><td>4</td><td><a href=\"B.txt\">B.txt</a></td>\n" +
                "<td>0.020869565217391348</td></tr>\n" +
                "<tr><td>5</td><td><a href=\"A.txt\">A.txt</a></td>\n" +
                "<td>0.020869565217391348</td></tr>");
        String text = tp.replace(varass);
        System.out.println(text);
    }
}
