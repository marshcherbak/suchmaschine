public class CRCTest {
    public static void main(String[] args) {
        CRC crc  = new CRC(38);
        int m = crc.crcASCIIString("az");
        System.out.println("38" + " az: " +m);
        m = crc.crcASCIIString("pinguin");
        System.out.println("38" + " pinguin: " + m);
        m = crc.crcASCIIString("oy");
        System.out.println("38" + " oy: " +m);
        crc = new CRC(2147483647);
        m = crc.crcASCIIString("az");
        System.out.println("2147483647" + " az: " +m);
        crc = new CRC(-2147483647);
        m = crc.crcASCIIString("hey");
        System.out.println("-2147483647" + " hey: " +m);

        crc = new CRC(40);
        m = crc.crcASCIIString("poodles");
        System.out.println("40" + " poodles: " + m);

    }
}
