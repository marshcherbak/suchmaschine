public class TestIt {
    /* add document to collection*/
    private static void addTest(LinkedDocumentCollection dc, String command){
        int j = 0;
        String title = "";
        while(command.charAt(j)!=':'){
            title += command.charAt(j);
            j++;
        }
        String content = command.substring(j+1);
        LinkedDocument d = new LinkedDocument(title, "en", "sum", new Date(),
                new Author("FN", "LN", new Date(), "res", "@"),content, title);
        dc.appendDocument(d);
    }
    /* list all docs in collection*/
    private static void listTest(LinkedDocumentCollection dc){
        DocumentCollectionCell temp = dc.getDocuments();
        while(temp!=null){
            System.out.println(temp.getInfo().getTitle());
            temp = temp.getNext();
        }
    }

    /* count given word in each document in the collection*/
    private static void countTest(LinkedDocumentCollection dc, String command){
        int k = 0;
        String word = "";
        while(k < command.length()){
            word += command.charAt(k);
            k++;
        }
        DocumentCollectionCell temp2 = dc.getDocuments();
        while(temp2!=null){
            WordCountsArray wc = temp2.getInfo().getWordCounts();
            int check = wc.getIndexOfWord(word);
            if(check==-1)
                System.out.println("\"" + word + "\"" + " in " + "\"" + temp2.getInfo().getTitle() + "\" : " + "not at all.");
            else {
                int count = wc.getCount(check);
                System.out.println("\"" + word + "\"" + " in " + "\"" + temp2.getInfo().getTitle() + "\" : x" + count + ".");
            }
            temp2 = temp2.getNext();
        }
    }

    /* tests match and prints the results*/
    private static void queryTest(LinkedDocumentCollection dc, String search){
        dc.match(search);
        DocumentCollectionCell temp3 = dc.getDocuments();
        int l = 1;
        while(temp3!=null){
            System.out.println(l + ". " + temp3.getInfo().getTitle() + ";    Relevance = " + dc.getRelevance(l-1));
            temp3 = temp3.getNext();
            l++;
        }
    }
    public static void main(String[] args) {
        LinkedDocumentCollection dc = new LinkedDocumentCollection();
        String command = "";
        while(!command.equals("exit")){
            command = Terminal.askString("> ");
            String s = "";
            int i = 0;
            while(i < command.length() && command.charAt(i)!=' '){
                s+=command.charAt(i);
                i++;
            }
            if(i<command.length()-1)
                command = command.substring(i+1);
            switch(s){
                case "exit":
                    return;
                case "add":
                    addTest(dc, command);
                    break;
                case "list":
                    listTest(dc);
                    break;
                case "count":
                    countTest(dc, command);
                    break;
                case "query":
                    queryTest(dc, command);
                    break;
                case "crawl":
                    LinkedDocumentCollection res = dc.crawl();
                    DocumentCollectionCell temp = res.getDocuments();
                    while(temp!=null){
                        dc.appendDocument(temp.getInfo());
                        temp = temp.getNext();
                    }
                    break;
                case "pageRank":
                    double [] PR2 = dc.pageRank(0.85);
                    DocumentCollectionCell temp3 = dc.getDocuments();
                    int p=0;
                    while(temp3!=null){
                        System.out.println((p+1) + ". " + temp3.getInfo().getTitle() + ";    RageRank = " + PR2[p]);
                        temp3 = temp3.getNext();
                        p++;
                    }
                    break;
                default:
                    break;
            }

        }
    }

}

/*   Test 1 files: Luke, Rory, Kirk, Paris, Lane, EmilyGilmore
Luke:
Luke quotes
link:Lorelai Have you tried the insane asylum where everybody in this room is supposed to be? link:Kirk Out back in the dumpster

Rory:
Rory quotes
link:Paris God, you're like a pop-up book from hell link:Lane When are you going to let your parents know that you listen to the evil rock music? link:EmilyGilmore I can't believe you found the recipe for Beefaroni

Paris:
Paris quotes
link:Lorelai I could really call you?

Lane:
Lane quotes
link:Rory hey you lasted one more why than I did

Kirk:
Kirk quotes
link:Luke Luke, where's your lost and found

EmilyGilmore:
Emily Gilmore Quotes
link:Lorelai link:Rory When a woman gives birth to a crack baby you do not buy her a puppy


 Results of the test:

> add Lorelai:link:Luke burger boy dance link:Rory Yes, and the lesson we have learned from that is you should never become a spy
> crawl
> pageRank
1. Lorelai;    RageRank = 0.21536296881597347
2. Luke quotes ;    RageRank = 0.20535755677196596
3. Kirk quotes;    RageRank = 0.10870549065530079
4. Rory quotes;    RageRank = 0.21961531134940868
5. Paris quotes ;    RageRank = 0.08365289080245007
6. Lane quotes ;    RageRank = 0.08365289080245007
7. Emily Gilmore Quotes;    RageRank = 0.08365289080245007
> query you
1. Rory quotes;    Relevance = 0.15408112632812584
2. Lorelai;    Relevance = 0.11586749861460416
3. Luke quotes ;    Relevance = 0.11150340534484288
4. Lane quotes ;    Relevance = 0.07448569769618132
5. Emily Gilmore Quotes;    Relevance = 0.06416653759296825
6. Kirk quotes;    Relevance = 0.04348219626212031
7. Paris quotes ;    Relevance = 0.033461156320980026
> exit

Process finished with exit code 0
*/

/* Test 2: Documents: BB, CC, DD, EE, FF
BB:
BB
link:CC

CC:
CC
link:DD

DD:
DD
link:CC link:EE

EE:
EE
link:FF

FF:
FF
link:BB link:AA

> add AA:link:BB
> crawl
> pageRank
1. AA;    RageRank = 0.08217306964056446
2. BB;    RageRank = 0.15202019535991285
3. CC;    RageRank = 0.2580698366125418
4. DD;    RageRank = 0.24435934459579195
5. EE;    RageRank = 0.12885270426734816
6. FF;    RageRank = 0.13452484952384164
> query
1. CC;    Relevance = 0.10322793464501673
2. DD;    Relevance = 0.09774373783831679
3. BB;    Relevance = 0.06080807814396514
4. FF;    Relevance = 0.05380993980953666
5. EE;    Relevance = 0.05154108170693927
6. AA;    Relevance = 0.032869227856225786
> exit

Process finished with exit code 0
*/

/* documents from Blatt 8 + gilmore girls docs
> add b.txt:link:a.txt link:e.txt
> crawl
> pageRank
1. b.txt;    RageRank = 0.14897680763989074
2. a.txt;    RageRank = 0.0933151432470439
3. c.txt;    RageRank = 0.3429150434218966
4. d.txt;    RageRank = 0.32147786244412807
5. e.txt;    RageRank = 0.0933151432470439
> add Lorelai:link:Luke burger boy dance link:Rory Yes, and the lesson we have learned from that is you should never become a spy
> crawl
> pageRank
1. b.txt;    RageRank = 0.06207366985007336
2. a.txt;    RageRank = 0.03888130968651721
3. c.txt;    RageRank = 0.1428812399966597
4. d.txt;    RageRank = 0.13394913744689907
5. e.txt;    RageRank = 0.03888130968651721
6. Lorelai;    RageRank = 0.12562839848158416
7. Luke quotes ;    RageRank = 0.11979188940066252
8. Kirk quotes;    RageRank = 0.06341155299528162
9. Rory quotes;    RageRank = 0.12810891484097572
10. Paris quotes ;    RageRank = 0.04879752587160971
11. Lane quotes ;    RageRank = 0.04879752587160971
12. Emily Gilmore Quotes;    RageRank = 0.04879752587160971
> query you
1. Rory quotes;    Relevance = 0.16259030984375836
2. Lorelai;    Relevance = 0.10103745795573746
3. Luke quotes ;    Relevance = 0.09783189351279742
4. Lane quotes ;    Relevance = 0.0901249523664759
5. Emily Gilmore Quotes;    Relevance = 0.07291482726184641
6. c.txt;    Relevance = 0.05715249599866382
7. d.txt;    Relevance = 0.05357965497875963
8. Kirk quotes;    Relevance = 0.02536462119811264
9. b.txt;    Relevance = 0.024829467940029402
10. Paris quotes ;    Relevance = 0.01951901034864389
11. a.txt;    Relevance = 0.015552523874606889
12. e.txt;    Relevance = 0.015552523874606889
> exit

Process finished with exit code 0
*/

/*
  documents from example in Blatt08
  Task:
  Connected to the target VM, address: '127.0.0.1:57619', transport: 'socket'
  > add b.txt:link:a.txt link:e.txt
  > crawl
  > pageRank
  1. b.txt;    RageRank = 0.14897680763989074
  2. a.txt;    RageRank = 0.0933151432470439
  3. c.txt;    RageRank = 0.3429150434218966
  4. d.txt;    RageRank = 0.32147786244412807
  5. e.txt;    RageRank = 0.0933151432470439
  > query einmal
  1. a.txt;    Relevance = 0.28591770952746826
  2. c.txt;    Relevance = 0.13716601736875864
  3. d.txt;    Relevance = 0.12859114497765123
  4. b.txt;    Relevance = 0.0595907230559563
  5. e.txt;    Relevance = 0.037326057298817564
  > exit
  Disconnected from the target VM, address: '127.0.0.1:57619', transport: 'socket'

  Process finished with exit code 0
  */
/*
* */