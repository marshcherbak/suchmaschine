public class RecursiveDeterminant {
    /**
     * calculates the determinant off matrix 2x2*/
    public static int det2x2(int[][] matrix){
        int res = 0;
        if(matrix!=null && matrix.length==2){
            if(matrix.length == 2 && matrix[0].length==2){
                res = matrix[0][0]*matrix[1][1]-matrix[0][1]*matrix[1][0];
            }
        }
        return res;
    }

    /**
     * calculates the det of matrix 383 by reducing to minors
     * and calculatin 2x2*/
    public static int det3x3(int[][] matrix){
        int res = 0;
        if(matrix!=null && matrix.length == 3){
            int a = matrix[0][0];
            int b = matrix[0][1];
            int c = matrix[0][2];
            int[][]det = removeRow(matrix, 0);
            int[][]det1 = removeColumn(det,0);
            a = a*det2x2(det1);

            det1 = removeColumn(det, 1);
            b = b*det2x2(det1);

            det1 = removeColumn(det, 2);
            c = c*det2x2(det1);

            res = a-b+c;
        }
        return res;
    }

    /**
     * removes a row ar rowIndex from matrix and returns a new matrix
     * without this row
     * if rowIndex illegal or matrix is null returns null*/
    public static int[][] removeRow(int[][] matrix, int rowIndex){
        if(matrix == null || rowIndex < 0 || rowIndex >=matrix.length)
            return null;

        int[][] res = new int[matrix.length-1][matrix[0].length];
        int k = 0;
        for(int i = 0; i < matrix.length; i++){
            if(i!=rowIndex) {
                for (int j = 0; j < matrix.length; j++) {
                    res[k][j] = matrix[i][j];
                }
                k++;
            }
        }
        return res;
    }

    /**
     * removes colIndex column from matrix
     * and returns a new matrix without this column
     * if colindex illegal or matrix null returns null*/
    public static int[][] removeColumn(int[][] matrix, int colIndex){
        if(matrix == null || colIndex < 0 || colIndex >=matrix[0].length)
            return null;

        int[][] res = new int[matrix.length][matrix[0].length-1];
        for(int i = 0; i < matrix.length; i++){
            int k = 0;
            for (int j = 0; j < matrix[0].length; j++) {
                if(j!=colIndex) {
                    res[i][k] = matrix[i][j];
                    k++;
                }
            }
        }
        return res;
    }

    /**
     * recursively calculates determinant of matrix NxN by getting minors
     * (removing columns/rows) and calculating det of smaller matrix
     * bottom is when n = 3 and we can use det3x3*/
    public int getNxN(int [][] matrix){
        int res = 0;
        if(matrix.length == 3)
            return det3x3(matrix);
        int[][] withoutFirst = removeRow(matrix, 0);
        for(int i = 0; i < matrix[0].length; i++){
            int[][] a = removeColumn(withoutFirst, i);
            res+=Math.pow(-1, i)*matrix[0][i]*getNxN(a);
        }
        return res;
    }

    public static void main(String[] args) {
        int [][] a = { {3,5,1}, {1,4,2}, {7,1,9}};
        RecursiveDeterminant r = new RecursiveDeterminant();
        int k = r.getNxN(a);
        System.out.println(k);

        int [][] b = { {4,3,2,2}, {0,1,-3,3}, {0,-1,3,3}, {0,3,1,1}};
        k = r.getNxN(b);
        System.out.println(k);

        int [][] c = { {4,3,2,2}, {0,1,0,-2}, {1,-1,3,3}, {2,3,1,1}};
        k = r.getNxN(c);
        System.out.println(k);

        int [][] d = { {0,6,-2,-1,5}, {0,0,0,-9,-7}, {0,15,35,0,0}, {0,-1,-11,-2,1}, {-2,-2,3,0,-2}};
        k = r.getNxN(d);
        System.out.println(k);
    }
}
